{
	 calculator -- used to compute the results
	 version 1.0, November 17th, 2012

	 Copyright (C) 2012-2013 Florea Marius Florin
	
	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.

	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
}

(* contains functions for computing results *)


{$mode objfpc}
{$h+}
{$smartlink on}


unit calculator;


interface


	uses
		sysutils, math,
		myTypes, error;


	(* evaluates the input from data and stores in data the result of that evaluation *)
	procedure compute_format(settings: pprogramSettings; data: pcompData);


implementation


	const
		(* the limit for stacks, how many elements it accepts *)
		LIMIT = 500;
		e = 2.7182818284590452;


	var
		postfixStack: array[1..LIMIT] of double;	// stack on which the computations are made
		postfixSp: integer;		// postfix stack position
		infixStack: array[1..LIMIT] of string;	// stack that holds operators when transforming from infix to postfix
		infixSp: integer;	// infix stack position


// auxiliary stuff ---------------------------------------------------------------------------------

	// replaces all occurences of 'toReplace' with the 'replacement' in the 'str' string
	procedure replace_all(var str: string; toReplace, replacement: string);
		var
			i: integer;
		begin
			repeat
				i:= pos(toReplace, str);
				if (i > 0) then begin
					insert(replacement, str, i);
					delete(str, i+length(replacement), length(toReplace));
				end;
			until (i = 0);
		end;


	function arccot(x: double): double;
		begin
			result:= (pi/2)-arctan(x);
		end;


	function cotanh(x: double): double;
		begin
			result:= cosh(x)/sinh(x);
		end;


	function arccoth(x: double): double;
		begin
			result:= (1/2)*ln((x+1)/(x-1));
		end;


	(* checks to see if the value for tangent is a multiple of pi/2 *)
	function tan_is_asym(settings: pprogramSettings; x: double): boolean;
		var
			i: integer;
			t: double;
			d: boolean = false;
		begin
			i:= 1;
			case settings^.trigUnits of
				1 : begin	// tan is in degrees
						if (x > 0.0) then begin
							repeat
								t:= i*90;
								if (abs(t-x) < 0.0000001) then
									d:= true;
								i:= i+2;
							until (t > x);
						end else begin
							repeat
								t:= -i*90;
								if (abs(t-x) < 0.0000001) then
									d:= true;
								i:= i+2;
							until (t < x);
						end;
					end;	// 1st label end
				2 : begin	// tan is in radians
						if (x > 0.0) then begin
							repeat
								t:= i*pi/2;
								// if higher precision is used then we will get
								//wrong results when making graphs
								if (abs(t-x) < 0.01) then
									d:= true;
								i:= i+2;
							until (t > x);
						end else begin
							repeat
								t:= -i*pi/2;
								if (abs(t-x) < 0.01) then
									d:= true;
								i:= i+2;
							until (t < x);
						end;
					end;	// 2nd label end
				end;	// case end
			
			result:= d;
		end;


	(* checks to see if the value for cotangent is a multiple of pi *)
	function cotan_is_asym(settings: pprogramSettings; x: double): boolean;
		var
			i: integer;
			t: double;
			d: boolean = false;
		begin
			i:= 0;
			case settings^.trigUnits of
				1 : begin	// cotan is in degrees
						if (x > 0.0) then begin
							repeat
								t:= i*180;
								if (abs(t-x) < 0.0000001) then
									d:= true;
								inc(i);
							until (t > x);
						end else begin
							repeat
								t:= -i*180;
								if (abs(t-x) < 0.0000001) then
									d:= true;
								inc(i);
							until (t < x);
						end;
					end;	// 1st label end
				2 : begin	// cotan is in radians
						if (x > 0.0) then begin
							repeat
								t:= i*pi;
								if (abs(t-x) < 0.01) then
									d:= true;
								inc(i);
							until (t > x);
						end else begin
							repeat
								t:= -i*pi;
								if (abs(t-x) < 0.01) then
									d:= true;
								inc(i);
							until (t < x);
						end;
					end;	// 2nd label end
				end;	// case end

			result:= d;
		end;


	(* replaces funcs names with symbols *)
	procedure transform_input(settings: pprogramSettings; data: pcompData);
		begin
			case settings^.language of
				1 : begin
						replace_all(data^.input, 'sqrt', '`');
						replace_all(data^.input, 'arccoth', '|');
						replace_all(data^.input, 'acoth', '|');
						replace_all(data^.input, 'arccot', '}');
						replace_all(data^.input, 'acot', '}');
						replace_all(data^.input, 'coth', '<');
						replace_all(data^.input, 'cot', ',');
						replace_all(data^.input, 'arctanh', '\');
						replace_all(data^.input, 'atanh', '\');
						replace_all(data^.input, 'arctan', '{');
						replace_all(data^.input, 'atan', '{');
						replace_all(data^.input, 'tanh', '"');
						replace_all(data^.input, 'tan', '&');
					end;	// 1st label end
				2 : begin
						replace_all(data^.input, 'rad', '`');
						replace_all(data^.input, 'actgh', '|');
						replace_all(data^.input, 'actg', '}');
						replace_all(data^.input, 'ctgh', '<');
						replace_all(data^.input, 'ctg', ',');
						replace_all(data^.input, 'atgh', '\');
						replace_all(data^.input, 'atg', '{');
						replace_all(data^.input, 'tgh', '"');
						replace_all(data^.input, 'tg', '&');
					end;	// 2nd label end
			end;	// case end

			replace_all(data^.input, 'log', '~');
			replace_all(data^.input, 'ln', '!');
			replace_all(data^.input, 'abs', '%');
			replace_all(data^.input, 'arcsinh', '>');
			replace_all(data^.input, 'asinh', '>');
			replace_all(data^.input, 'arcsin', '[');
			// arcsin is sometimes shortened as asin
			replace_all(data^.input, 'asin', '[');
			replace_all(data^.input, 'sinh', ';');
			replace_all(data^.input, 'sin', '@');
			replace_all(data^.input, 'arccosh', '?');
			replace_all(data^.input, 'acosh', '?');
			replace_all(data^.input, 'arccos', ']');
			// arccos is sometimes shortened as acos
			replace_all(data^.input, 'acos', ']');
			replace_all(data^.input, 'cosh', ':');
			replace_all(data^.input, 'cos', '$');
			replace_all(data^.input, 'pi', 'p');
		end;
//--------------------------------------------------------------------------------------------------	


// postfix stuff -----------------------------------------------------------------------------------
	(* pushes value of 'x' on postfixStack or returns an error in data.err *)
	procedure postfix_push(data: pcompData; x: double);
		begin
			if (postfixSp < LIMIT) then begin
				inc(postfixSp);
				postfixStack[postfixSp]:= x;
			end else begin
				data^.err:= 1;
			end;
		end;


	(* pops element from postfixStack or returns error in data.err *)
	function postfix_pop(data: pcompData): double;
		begin
			if (postfixSp > 0) then begin
				result:= postfixStack[postfixSp];
				dec(postfixSp);
			end else begin
				data^.err:= 2;
			end;
		end;


	(* gets the next operand/operator from the 'data.input' string.
	* It destroys the original string *)
	function postfix_get_operand(data: pcompData): string;
		var
			i: integer;
			d: string;
		begin
			// if it is an operator (+, *) then return the operator and
			// delete if from the string along with it's trailing white space
			if (pos(data^.input[1], '+*/^`~!@$&,pe[]{};:"<>?\|%#') > 0) then begin
				result:= data^.input[1];
				delete(data^.input, 1, 2);
				exit();
			end;

			// the minus operator can be either in it's binary form:
			// case when it takes 2 arguments and perfroms substraction (e.g.: 2-5)
			// or in it's unary form:
			// case when it means: the number following it is negative
			// this makes more sense in the infix procedure
			if (data^.input[1] = '-') then begin
				if (data^.input[2] = ' ') then begin
					// it is the binary minus
					result:= '-';
					delete(data^.input, 1, 2);
					exit();
				end else begin
					// it is the unary minus
					i:= 1; d:= '';
					while (data^.input[i] <> ' ') do begin
						d:= concat(d, data^.input[i]);
						inc(i);
					end;
					
					result:= d;
					delete(data^.input, 1, i);
					exit();
				end;
			end;

			// if it is a number then get the number and delete it
			// from the string along with it's trailing white space
			i:= 1; d:= '';
			while (data^.input[i] <> ' ') do begin
				d:= concat(d, data^.input[i]);
				inc(i);
			end;

			result:= d;
			delete(data^.input, 1, i);
		end;


	(* the reverse polish notation calculator 
	* 
	* sign meanings:
	* _ : unary minus
	* ` : sqrt 
	* ~ : log 
	* ! : ln 
	* % : abs
	* @ : sin
	* ; : sinh
	* [ : arcsin
	* > : arcsinh
	* $ : cos
	* : : cosh
	* ] : arccos
	* ? : arccosh
	* & : tan
	* " : tanh
	* { : arctan
	* \ : arctanh
	* , : cot
	* < : coth
	* } : arccot
	* | : arccoth*)
	function postfix_calculator(settings: pprogramSettings; data: pcompData): double;
		var
			dr, dr2: double;
			d: string;
			err: integer;
		begin
			postfixSp:= 0;
			data^.input:= concat(data^.input, ' #');	// '#' marks end of input
			
			while (length(data^.input) > 0) do begin
				// if there were errors during evaluation then exit evaluation
				// and tell the user what error occured
				if ((data^.err <> 0) and (data^.suppressErr = 1)) then begin
					if (settings^.mode = 2) then
						raise_error_cli(settings, data^.err);
					exit();
				end;

				d:= postfix_get_operand(data);
				
				case d of
					'p' : // pi
						begin
							postfix_push(data, pi);
						end;
					'e' : // euler's number
						begin
							postfix_push(data, e);
						end;
					'`' : // sqrt
						begin
							dr:= postfix_pop(data);

							try
								postfix_push(data, sqrt(dr));
							except
								data^.err:= 7;
							end;
						end;
					'~' : // log
						begin
							try
								postfix_push(data, log10(postfix_pop(data)));
							except
								on e: exception do
									if (e.message = 'Division by zero') then
										data^.err:= 8
									else if ((e.message = 'Access violation') or
											 (e.message = 'Invalid floating point operation')) then
										data^.err:= 9
									else
										data^.err:= 255;
							end;
						end;
					'!' : // ln
						begin
							try
								postfix_push(data, ln(postfix_pop(data)));
							except
								on e: exception do
									if (e.message = 'Division by zero') then
										data^.err:= 8
									else if ((e.message = 'Access violation') or
											 (e.message = 'Invalid floating point operation')) then
										data^.err:= 9
									else
										data^.err:= 255;
							end;
						end;
					'%' : // abs
						begin
							postfix_push(data, abs(postfix_pop(data)));
						end;
					'@' : // sin
						begin
							case settings^.trigUnits of
								1 : postfix_push(data, sin(degToRad(postfix_pop(data))));
								2 : postfix_push(data, sin(postfix_pop(data)));
							end;
						end;
					';' : // sinh
						begin
							case settings^.trigUnits of
								1 : postfix_push(data, sinh(degToRad(postfix_pop(data))));
								2 : postfix_push(data, sinh(postfix_pop(data)));
							end;
						end;
					'[' : // arcsin
						begin
							case settings^.trigUnits of
								1 : try
										postfix_push(data, arcsin(degToRad(postfix_pop(data))));
									except
										data^.err:= 12;
									end;
								2 : try
										postfix_push(data, arcsin(postfix_pop(data)));
									except
										data^.err:= 12;
									end;
							end;
						end;
					'>' : // arcsinh
						begin
							case settings^.trigUnits of
								1 : postfix_push(data, arsinh(degToRad(postfix_pop(data))));
								2 : postfix_push(data, arsinh(postfix_pop(data)));
							end;
						end;
					'$' : // cos
						begin
							case settings^.trigUnits of
								1 : postfix_push(data, cos(degToRad(postfix_pop(data))));
								2 : postfix_push(data, cos(postfix_pop(data)));
							end;
						end;
					':' : // cosh
						begin
							case settings^.trigUnits of
								1 : postfix_push(data, cosh(degToRad(postfix_pop(data))));
								2 : postfix_push(data, cosh(postfix_pop(data)));
							end;
						end;
					']' : // arccos
						begin
							case settings^.trigUnits of
								1 : try
										postfix_push(data, arccos(degToRad(postfix_pop(data))));
									except
										data^.err:= 13;
									end;
								2 : try
										postfix_push(data, arccos(postfix_pop(data)));
									except
										data^.err:= 13;
									end;
							end;
						end;
					'?' : // arccosh
						begin
							case settings^.trigUnits of
								1 : try
										postfix_push(data, arcosh(degToRad(postfix_pop(data))));
									except
										data^.err:= 15;
									end;
								2 : try
										postfix_push(data, arcosh(postfix_pop(data)));
									except
										data^.err:= 15;
									end;
							end;
						end;
					'&' : // tan
						begin
							dr:= postfix_pop(data);

							if (tan_is_asym(settings, dr)) then begin
								data^.err:= 10;
							end else begin
								case settings^.trigUnits of
									1 : postfix_push(data, tan(degToRad(dr)));
									2 : postfix_push(data, tan(dr));
								end;
							end;
						end;
					'"' : // tanh
						begin
							case settings^.trigUnits of
								1 : postfix_push(data, tanh(degToRad(postfix_pop(data))));
								2 : postfix_push(data, tanh(postfix_pop(data)));
							end;
						end;
					'{' : // arctan
						begin
							case settings^.trigUnits of
								1 : postfix_push(data, arctan(degToRad(postfix_pop(data))));
								2 : postfix_push(data, arctan(postfix_pop(data)));
							end;
						end;
					'\' : // arctanh
						begin
							case settings^.trigUnits of
								1 : try
										postfix_push(data, artanh(degToRad(postfix_pop(data))));
									except
										data^.err:= 16;
									end;
								2 : try
										postfix_push(data, artanh(postfix_pop(data)));
									except
										data^.err:= 16;
									end;
							end;
						end;
					',' : // cot
						begin
							dr:= postfix_pop(data);

							if (cotan_is_asym(settings, dr)) then begin
								data^.err:= 11;
							end else begin
								case settings^.trigUnits of
									1 : postfix_push(data, cotan(degToRad(dr)));
									2 : postfix_push(data, cotan(dr));
								end;
							end;
						end;
					'<' : // coth
						begin
							case settings^.trigUnits of
								1 : try
										postfix_push(data, cotanh(degToRad(postfix_pop(data))));
									except
										data^.err:= 14;
									end;
								2 : try
										postfix_push(data, cotanh(postfix_pop(data)));
									except
										data^.err:= 14;
									end;
							end;
						end;
					'}' : // arccot
						begin
							case settings^.trigUnits of
								1 : postfix_push(data, arccot(degToRad(postfix_pop(data))));
								2 : postfix_push(data, arccot(postfix_pop(data)));
							end;
						end;
					'|' : // arccoth
						begin
							case settings^.trigUnits of
								1 : try
										postfix_push(data, arccoth(degToRad(postfix_pop(data))));
									except
										data^.err:= 17;
									end;
								2 : try
										postfix_push(data, arccoth(postfix_pop(data)));
									except
										data^.err:= 17;
									end;
							end;
						end;
					'^' :
						begin
							dr:= postfix_pop(data);
							dr2:= postfix_pop(data);

							try
								postfix_push(data, power(dr2, dr));
							except
								on e: exception do
									if (e.message = 'Division by zero') then
										data^.err:= 5
									else if (e.message = 'Invalid argument') then
										data^.err:= 6;
									else
										data^.err:= 255;
							end;
						end;
					'*' :
						begin
							postfix_push(data, postfix_pop(data) * postfix_pop(data));
						end;
					'/' :
						begin
							dr:= postfix_pop(data);

							try
								postfix_push(data, postfix_pop(data) / dr);
							except
								data^.err:= 3;
							end;
						end;
					'+' :
						begin
							postfix_push(data, postfix_pop(data) + postfix_pop(data));
						end;
					'-' :
						begin
							dr:= postfix_pop(data);

							postfix_push(data, postfix_pop(data) - dr);
						end;
					'#' :
						begin
							result:= postfixStack[1];
						end;
				else begin
					// it is a number, or we hope it is a number
					err:= 0;
					val(d, dr, err);
					if (err = 0) then begin
						postfix_push(data, dr);
					end else begin
						data^.err:= 4;
					end;
				end;	// case else end
				end;	// case end
			end;	// while end
		end;

//--------------------------------------------------------------------------------------------------


// infix stuff--------------------------------------------------------------------------------------

	(* pushes value of 'x' on infixStack or returns an error in data.err *)
	procedure infix_push(data: pcompData; x: string);
		begin
			if (infixSp < LIMIT) then begin
				inc(infixSp);
				infixStack[infixSp]:= x;
			end else begin
				data^.err:= 1;
			end;
		end;


	(* pops element from infix stack or return an error in data.err *)
	function infix_pop(data: pcompData): string;
		begin
			if (infixSp > 0) then begin
				result:= infixStack[infixSp];
				dec(infixSp);
			end else begin
				data^.err:= 2;
			end;
		end;


	(* gets the next operand/operator from the input string.
	* 'isFirst' is used to signal if the current element is the first
	* element of the string or the first element after an open paranthesis
	* (used to distinguish between unary and binary minus *)
	function infix_get_operand(data: pcompData; isFirst: boolean): string;
		var
			i: integer;
			d: string;
		begin
			// if it is an operator (+, *) then return the operator
			// and delete it from the string
			if (pos(data^.input[1], '+*/()^`~!@$&,pe[]{};:"<>?\|%#') > 0) then begin
				result:= data^.input[1];
				delete(data^.input, 1, 1);
				exit();
			end;

			// the minus operator can be either in it's binary form:
			// case when it takes two arguments and perform substraction (e.g.: 2-5);
			// or in it's unary form:
			// case when it means the number following it is negative (here) (e.g.: -4);
			if (data^.input[1] = '-') then begin
				// minus can be in it's unary form in infix evaluation only when
				// it is the first character of the string or it is immeadiately
				// after an open paranthesis
				if (isFirst) then begin
					// it is the unary minus
					result:= '_';
					delete(data^.input, 1, 1);
					exit();
				end else begin
					// it is the binary minus
					result:= '-';
					delete(data^.input, 1, 1);
					exit();
				end;
			end;


			// if it is a number get the number then delete from the string
			i:= 1; d:= '';
			while (pos(data^.input[i], '-+*/()^`~!@$&,pe[]{};:"<>?\|%#') = 0) do begin
				d:= concat(d, data^.input[i]);
				inc(i);
			end;
			
			result:= d;
			delete(data^.input, 1, i-1);
		end;


	(* converts the infix string to a postfix representation,
	* the one understood by postfix_calculator *)
	function infix_to_postfix(settings: pprogramSettings; data: pcompData): string;
		var
			isFirst: boolean = true;
			ds, res: string;
		begin
			data^.input:= concat(data^.input, '#');		// '#' marks end of input
			res:= '';

			while (length(data^.input) > 0) do begin
				// if there were errors during evaluation then exit evaluation
				// and tell the user what error occured
				if ((data^.err <> 0) and (data^.suppressErr = 1)) then begin
					if (settings^.mode = 2) then
						raise_error_cli(settings, data^.err);
					exit();
				end;

				ds:= infix_get_operand(data, isFirst);
				isFirst:= false;

				case ds of
					'p', 'e' :
						begin
							res:= concat(res, ds, ' ');
						end;
					'(' :
						begin
							infix_push(data, ds);
							isFirst:= true;
						end;
					')' :
						begin
							while ((infixSp > 0) and (infixStack[infixSp] <> '(')) do
								res:= concat(res, infix_pop(data), ' ');

							if (infixStack[infixSp] = '(') then begin
								infix_pop(data);
							end else begin
								data^.err:= 4;
							end;
						end;
					'`', '~', '!', '@', '$', '&', ',', '[', ']', '{', '}', ';',
					':', '"', '<', '>', '?', '\', '|', '%' :
						begin
							if (infixSp = 0) then begin
								infix_push(data, ds);
							end else begin
								while ((infixSp > 0) and
									(pos(infixStack[infixSp], '`~!@$&,[]{};:"<>?\|%') > 0)) do
									res:= concat(res, infix_pop(data), ' ');
								infix_push(data, ds);
							end;
						end;
					'^' :
						begin
							if (infixSp = 0) then begin
								infix_push(data, ds);
							end else begin
								while ((infixSp > 0) and
									(pos(infixStack[infixSp], '`~!@$&,[]{};:"<>?\|%') > 0)) do
									res:= concat(res, infix_pop(data), ' ');
								infix_push(data, ds);
							end;
						end;
					'*', '/' :
						begin
							if (infixSp = 0) then begin
								infix_push(data, ds);
							end else begin
								while ((infixSp > 0) and
									(pos(infixStack[infixSp], '*/^`~!@$&,[]{};:"<>?\|%') > 0)) do
									res:= concat(res, infix_pop(data), ' ');
								infix_push(data, ds);
							end;
						end;
					'+', '-' :
						begin
							if (infixSp = 0) then begin
								infix_push(data, ds);
							end else begin
								while ((infixSp > 0) and (infixStack[infixSp] <> '(')) do
									res:= concat(res, infix_pop(data), ' ');
								infix_push(data, ds);
							end;
						end;
					'_' :	// unary minus
						begin
							// add (-1) to result string
							res:= concat(res, '-1 ');
							// push * on the stack so the number/expression gets multiplied by (-1)
							infix_push(data, '*');
						end;
					'#' :
						begin
							while (infixSp > 0) do
								res:= concat(res, infix_pop(data), ' ');
							delete(res, length(res), 1);	// remove the last white space
							result:= res;
						end;
				else
					// it is a number
					res:= concat(res, ds, ' ');
				end;	// case end
			end;	// while end
		end;
//--------------------------------------------------------------------------------------------------


	procedure compute_format(settings: pprogramSettings; data: pcompData);
		var
			d: double;
			i: integer;
		begin
			// a little check, if there is a space in the infix string
			if ((pos(' ', data^.input) > 0) and (settings^.eval = 1))  then
				data^.err:= 4;
		
			transform_input(settings, data);
			case settings^.eval of
				1 : begin	// infix evaluation
						data^.input:= infix_to_postfix(settings, data);
						d:= postfix_calculator(settings, data);
						str(d:0:settings^.decimals, data^.res);
					end;
				2 : begin	// postfix evaluation
						d:= postfix_calculator(settings, data);
						str(d:0:settings^.decimals, data^.res);
					end;
			end;

			// remove 0 without value from result
			if (settings^.decimals > 0) then begin
				i:= length(data^.res);
				while (data^.res[i] = '0') do begin
					delete(data^.res, i, 1);
					dec(i);
				end;
				if (data^.res[i] = '.') then
					delete(data^.res, i, 1);
			end;
		end;


end.
