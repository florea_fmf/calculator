{
	 theme -- visual theme of the program
	 version 1.0, November 17th, 2012

	 Copyright (C) 2012-2013 Florea Marius Florin

	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.
	
	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
}

(* containes a custom style for the program
* as the default one is ugly *)


{$mode objfpc}
{$h+}
{$smartlink on}


unit theme;


interface


	uses
		classes, sysutils,
		fpg_main, fpg_base;


	type
		TcalcStyle = class(TfpgStyle)
			public
				constructor create; override;
				// buttons
				procedure drawButtonFace(aCanvas: TfpgCanvas; x, y, w, h: TfpgCoord;
					aFlags: TfpgButtonFlags); override;
				// menu
				procedure drawMenuRow(aCanvas: TfpgCanvas; r: TfpgRect;
					aFlags: TfpgMenuItemFlags); override;
		end;


implementation

	uses
		fpg_styleManager;


	constructor TcalcStyle.create;
		begin
			inherited create;
			fpgSetNamedColor(clWindowBackground, TfpgColor($eeeeec));	// light grey
		end;


	procedure TcalcStyle.drawButtonFace(aCanvas: TfpgCanvas; x, y, w, h: TfpgCoord;
		aFlags: TfpgButtonFlags);
		var
			r: TfpgRect;
		begin
			r.setRect(x, y, w, h);
			
			if (btfIsDefault in aFlags) then begin
				aCanvas.setColor(TfpgColor($7b7b7b));
				aCanvas.setLineStyle(1, lsSolid);
				aCanvas.drawRectangle(r);
				
				inflateRect(r, -1, -1);
				exclude(aFlags, btfIsDefault);
				fpgStyle.drawButtonFace(aCanvas, r.left, r.top, r.width, r.height, aFlags);
				exit;
			end;

			// clear the canvas
			aCanvas.setColor(clWindowBackground);
			aCanvas.fillRectangle(r);
			
			if ((btfFlat in aFlags) and (not(btfIsPressed in aFlags))) then
				exit;	// no need to go further

			// outer rectangle
			aCanvas.setLineStyle(1, lsSolid);
			aCanvas.setColor(TfpgColor($a6a6a6));	// medium gray
			aCanvas.drawRectangle(r);

			// so we don't paint over the border
			inflateRect(r, -1, -1);
			// now paint the face of the button
			if (btfIsPressed in aFlags) then
				aCanvas.gradientFill(r, TfpgColor($cccccc), TfpgColor($e4e4e4), gdVertical)
			else begin
				aCanvas.gradientFill(r, TfpgColor($fafafa), TfpgColor($e2e2e2), gdVertical);
				aCanvas.setColor(TfpgColor($cccccc));
				aCanvas.drawLine(r.right, r.top, r.right, r.bottom);	// right
				aCanvas.drawLine(r.right, r.bottom, r.left, r.bottom);	// bottom
			end;
		end;


	procedure TcalcStyle.drawMenuRow(aCanvas: TfpgCanvas; r: TfpgRect; aFlags: TfpgMenuItemFlags);
		begin
			inherited drawMenuRow(aCanvas, r, aFlags);
			if ((mifSelected in aFlags) and (not(mifSeparator in aFlags))) then
				aCanvas.gradientFill(r, TfpgColor($73d5ff), TfpgColor($00b3ff), gdVertical);
		end;



initialization
	fpgStyleManager.registerClass('calcStyle', TcalcStyle);

end.