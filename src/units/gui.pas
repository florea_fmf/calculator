{
	 gui -- gui of the program
	 version 1.0, November 17th, 2012

	 Copyright (C) 2012-2013 Florea Marius Florin

	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.

	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
}

(* contains the gui of the program *)


{$mode objfpc}
{$h+}
{$smartlink on}


unit gui;


interface

	uses
		myTypes;

	(* start the program in gui mode *)
	procedure start_gui(settings: pprogramSettings; data: pcompData);


implementation


	uses
		sysutils, classes,
		fpg_base, fpg_main, fpg_form, fpg_menu, fpg_edit, fpg_label,
			fpg_button, fpg_dialogs, fpg_radioButton, fpg_styleManager,
			fpg_imgfmt_jpg, fpImage, fpg_colorWheel, fpg_spinEdit,
			fpWriteJPEG, fpCanvas, fpImgCanv, fpg_panel,
		error, calculator, theme, graph;
	
	
	const
		(* current version of the program
		* note: x.y.z
		* x = major version - increment only if major changes took place;
		* y = minor version - increment if new functionality is added;
		* z = micro version - increment when bug fixes are added; *)
		version = '1.3.2';
	

	type
		(* the main window of the program *)
		TmainForm = class(TfpgForm)
			private
				winWidth, winHeight: word;

				(* program menu *)
				menuBar: TfpgMenuBar;
				menuCalculatorSubMenu: TfpgPopupmenu;
				menuMiscSubMenu: TfpgPopupMenu;

				(* where the input is entered *)
				editBox: TfpgEdit;

				(* a text label that what error occured
				* during computation, if any *)
				errorLabel: TfpgLabel;

				(* used to make the from buttons;
				* NOTE: buttons are 37-37 pixels in size, but they are placed
				* at 40-40 pixels distance between one another *)
				button: TfpgButton;

				(* these are the buttons used to store and retrieve
				* ecuations/results (whatever the user has put in them *)
				memButtons: array[1..9] of TfpgButton;
				(* last selected button, used know what value
				* to return or to delete when mr/m- is pressed *)
				selectedButton: byte;

				label_: TfpgLabel;	// dummy label

				(* menu items callbacks *)
				procedure mi_graph_cb(sender: Tobject);
				procedure mi_options_cb(sender: Tobject);
				procedure mi_quit_cb(sender: Tobject);
				procedure mi_license_cb(sender: Tobject);
				procedure mi_credits_cb(sender: Tobject);

				(* gets the text from the input box and tries to compute
				* a result when the return key or '=' button is pressed *)
				procedure editBox_on_change_cb(sender: Tobject; var aText: word;
					var shiftState: TshiftState; var consumed: boolean);

				(* callback called when a button is pressed *)
				procedure button_press_cb(sender: Tobject);

				(* callback for memory buttons *)
				procedure memButton_press_cb(sender: Tobject);
			public
				constructor create(aOwner: Tcomponent); override;
				procedure afterCreate(); override;
		end;


		(* the options window *)
		ToptionsForm = class(TfpgForm)
			private
				button: TfpgButton;
				editBox: TfpgSpinEdit;
				rbutton: TfpgRadioButton;
				label_: TfpgLabel;

				procedure button_press_cb(sender: Tobject);
			public
				constructor create(aOwner: Tcomponent); override;
				procedure afterCreate(); override;
		end;


		(* the window displaying the graph, and various
		* controls for controling (dafuq?) the graph *)
		TgraphForm = class(TfpgForm)
			private
				(* displays what error occured *)
				errorLabel: TfpgLabel;
				(* the image containing the resulting graph *)
				graphImg: TfpgImage;
				(* where the func is entered *)
				funcInput: TfpgEdit;
				(* the range of the func *)
				funcRange: TfpgSpinEdit;
				(* the size of the graph *)
				graphSize: TfpgSpinEdit;
				(* used to draw the graph, save the graph and close the form *)
				button: TfpgButton;
				(* the size of the saved image *)
				imgSize: TfpgRadioButton;
				(* the type of the saved image *)
				imgType: TfpgRadioButton;
				label_: TfpgLabel;

				(* used for making the image displayed on the choose color button *)
				writer: TfpCustomImageWriter;
				img: TfpCustomImage;
				canv: TfpCustomCanvas;

				(* gets the input from the edit box containing
				* the function to graph *)
				procedure funcInput_on_keypress_cb(sender: Tobject; var aText: word;
					var shiftState: TshiftState; var consumed: boolean);
				procedure funcInput_on_change_cb(sender: Tobject);

				(* gets the range of the function *)
				procedure get_func_range_cb(sender: Tobject);

				(* gets the color used to draw the func *)
				procedure get_func_color_cb(sender: Tobject);

				(* gets the data for the image to save *)
				procedure get_image_data_cb(sender: Tobject);

				(* loads the image with the graph *)
				procedure load_image_cb(sender: Tobject);
				(* draws the image *)

				procedure form_paint(sender: Tobject);
				(* closes the form *)
				procedure button_press_cb(sender: Tobject);

				(* updates the image color to reflect the one choosen by user *)
				procedure update_images();

				(* registers the images used on choose color buttons *)
				procedure register_images();
				(* deregister an image, so the image on the button can update *)
				procedure deregister_images();
			public
				constructor create(aOwner: Tcomponent); override;
				destructor destroy; override;
				procedure afterCreate(); override;
		end;


		(* contains the color chooser dialog (colorwheel) *)
		TgraphColorForm = class(TfpgForm)
			private
				label_: TfpgLabel;

				colorWheel: TfpgColorWheel;
				valBar: TfpgValueBar;
				button: TfpgButton;
				preview: TfpgBevel;

				// hue, saturation, value edits
				edH, edS, edV: TfpgEdit;

				// rgb edits
				edR, edG, edB: TfpgEdit;

				FViaRGB: boolean;	// to prevent recursive changes

				procedure quit(sender: Tobject);

				procedure color_changed_cb(sender: Tobject);

				procedure update_HSV_components_cb(sender: Tobject);

				procedure update_RGB_components_cb(sender: Tobject);

				procedure RGB_changed_cb(sender: Tobject);

				procedure set_color_cb(sender: Tobject);
			public
				constructor create(aOwner: Tcomponent); override;
				procedure afterCreate(); override;
		end;


	var
		settings: pprogramSettings;
		data: pcompData;
		graphData: pgraphData;


	// main form -----------------------------------------------------------------------------------
	constructor TmainForm.create(aOwner: Tcomponent);
		begin
			winWidth:= 250;
			winHeight:= 340;

			inherited create(aOwner);
		end;


	procedure TmainForm.afterCreate();
		const
			buttonSize = 37;
		var
			i: byte;
			s: string;
		begin
			// main window propoerties
			name:= 'mainForm';
			width:= winWidth;
			height:= winHeight;
			minWidth:= winWidth;
			minHeight:= winHeight;
			windowPosition:= wpOneThirdDown;
			windowTitle:= 'Calculator';

			// menu
			menuBar:= TfpgMenuBar.create(self);
			with menuBar do begin
				name:= 'menuBar';
				setPosition(0, 0, winWidth, 25);
			end;

			menuCalculatorSubMenu:= TfpgPopupMenu.create(self);
			with menuCalculatorSubMenu do begin
				name:= 'menuCalcualtorSubMenu';
				case (settings^.language) of
					1: begin
						addMenuItem('Graph', '', @mi_graph_cb);
						addMenuItem('Options', '', @mi_options_cb);
						addMenuItem('-', '', nil);
						addMenuItem('Quit', '', @mi_quit_cb);
					   end;
					2: begin
						addMenuItem('Grafic', '', @mi_graph_cb);
						addMenuItem('Optiuni', '', @mi_options_cb);
						addMenuItem('-', '', nil);
						addMenuItem('Iesire', '', @mi_quit_cb);
					   end;
				end;
			end;

			menuMiscSubMenu:= TfpgPopupMenu.create(self);
			with menuMiscSubMenu do begin
				name:= 'menuMiscSubMenu';
				case (settings^.language) of
					1: begin
						addMenuItem('License', '', @mi_license_cb);
						addMenuItem('Credits', '', @mi_credits_cb);
					   end;
					2: begin
						addMenuItem('Licenta', '', @mi_license_cb);
						addMenuItem('Credite', '', @mi_credits_cb);
					   end;
				end;
			end;

			// attach submenus to main menu bar
			menuBar.addMenuItem('Calculator', nil).subMenu:= menuCalculatorSubMenu;
			menuBar.addMenuItem('Misc', nil).subMenu:= menuMiscSubMenu;


			// input box
			editBox:= TfpgEdit.create(self);
			with editBox do begin
				name:= 'editBox';
				autoSelect:= false;
				setFocus;
				setPosition(0, 25, winWidth, 40);
				fontDesc:= 'Sans-17:antialias=true';
				onKeyPress:= @editBox_on_change_cb;
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Enter here the equations and press "return" or'+
						'"=" button to get the result. Press "return" or "=" again'+
						' to get the entered equation.'
				else
					hint:= 'Introdu aici ecuatia si apasa "enter" sau butonul'+
						' "=" pentru a primi raspunsul. Apasa "enter" sau butonul'+
						' "=" pentru a primi ecuatia introdusa.';
			end;

			// error label
			errorLabel:= TfpgLabel.create(self);
			with errorLabel do begin
				name:= 'errorLabel';
				setPosition(0, 65, winWidth, 30);
				fontDesc:= 'Sans-9:antialias=true';
				wrapText:= true;
				text:= '';
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Errors are shown here.'
				else
					hint:= 'Aici sunt afisate erorile.';
			end;

			// buttons
			// 1st row------------------------
			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonClear';
				setPosition(5, 100, buttonSize, buttonSize);
				text:= 'clear';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonBcksp';
				setPosition(45, 100, buttonSize, buttonSize);
				text:= 'bcksp';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
				showHint:= true;
				hint:= 'Backspace';
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonSpace';
				setPosition(85, 100, buttonSize, buttonSize);
				text:= 'space';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonMs';
				setPosition(205, 100, buttonSize, buttonSize);
				text:= 'ms';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Show/hide items in memory.'
				else
					hint:= 'Arata/ascunde elementele din memorie.'
			end;
			//--------------------------------

			//2nd row ------------------------
			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrig';
				setPosition(5, 140, buttonSize, buttonSize);
				text:= 'trig';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Show/hide trigonometric functions.'
				else
					hint:= 'Arata/ascunde functiile trigonometrice.';
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonLog';
				setPosition(45, 140, buttonSize, buttonSize);
				text:= 'log';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonLn';
				setPosition(85, 140, buttonSize, buttonSize);
				text:= 'ln';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonPi';
				setPosition(125, 140, buttonSize, buttonSize);
				text:= UTF8String(#$CF#$80);
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;
			
			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonE';
				setPosition(165, 140, buttonSize, buttonSize);
				text:= 'e';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonMadd';
				setPosition(205, 140, buttonSize, buttonSize);
				text:= 'm+';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Adds the current items from input field to memory.'
				else
					hint:= 'Adauga continutul la input box in memorie.';
			end;
			//--------------------------------

			// 3rd row -----------------------
			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonPower';
				setPosition(5, 180, buttonSize, buttonSize);
				text:= '^';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonSqrt';
				setPosition(45, 180, buttonSize, buttonSize);
				text:= UTF8String(#$E2#$88#$9A);
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'button7';
				setPosition(85, 180, buttonSize, buttonSize);
				text:= '7';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'button8';
				setPosition(125, 180, buttonSize, buttonSize);
				text:= '8';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'button9';
				setPosition(165, 180, buttonSize, buttonSize);
				text:= '9';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonMsub';
				setPosition(205, 180, buttonSize, buttonSize);
				text:= 'm-';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Deletes the last selected item from memory.'
				else
					hint:= 'Sterge din memorie ultimul item selectat.';
			end;
			//--------------------------------

			// 4th row -----------------------
			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonMul';
				setPosition(5, 220, buttonSize, buttonSize);
				text:= UTF8String(#$C3#$97);
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonDiv';
				setPosition(45, 220, buttonSize, buttonSize);
				text:= UTF8String(#$C3#$B7);
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'button4';
				setPosition(85, 220, buttonSize, buttonSize);
				text:= '4';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;
			
			button:= TfpgButton.create(self);
			with button do begin
				name:= 'button5';
				setPosition(125, 220, buttonSize, buttonSize);
				text:= '5';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'button6';
				setPosition(165, 220, buttonSize, buttonSize);
				text:= '6';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonMr';
				setPosition(205, 220, buttonSize, buttonSize);
				text:= 'mr';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Retrieves last selected item from memory.'
				else
					hint:= 'Returneaza ultimul item selectat din memorie.';
			end;
			//--------------------------------

			// 5th row -----------------------
			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonAdd';
				setPosition(5, 260, buttonSize, buttonSize);
				text:= '+';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonSub';
				setPosition(45, 260, buttonSize, buttonSize);
				text:= '-';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'button1';
				setPosition(85, 260, buttonSize, buttonSize);
				text:= '1';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'button2';
				setPosition(125, 260, buttonSize, buttonSize);
				text:= '2';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'button3';
				setPosition(165, 260, buttonSize, buttonSize);
				text:= '3';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonMc';
				setPosition(205, 260, buttonSize, buttonSize);
				text:= 'mc';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Clears the memory.'
				else
					hint:= 'Sterge toata memoria.';
			end;
			//--------------------------------

			// 6th row -----------------------
			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonLpar';
				setPosition(5, 300, buttonSize, buttonSize);
				text:= '(';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonRpar';
				setPosition(45, 300, buttonSize, buttonSize);
				text:= ')';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'button0';
				setPosition(85, 300, buttonSize, buttonSize);
				text:= '0';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonPoint';
				setPosition(125, 300, buttonSize, buttonSize);
				text:= '.';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonEqual';
				setPosition(165, 300, buttonSize * 2 + 3, buttonSize);
				text:= '=';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;
			//--------------------------------

			// memory buttons ----------------
			label_:= TfpgLabel.create(self);
			with label_ do begin
				name:= 'memLabel';
				setPosition(winWidth+80, 0, 200, 30);
				if (settings^.language = 1) then
					text:= 'Memory'
				else
					text:= 'Memorie';
				fontDesc:= 'Sans-10:antialais=true';
			end;

			for i:= 1 to 9 do begin
				memButtons[i]:= TfpgButton.create(self);
				with memButtons[i] do begin
					str(i, s);
					name:= concat('memButtons', s);
					setPosition(winWidth+10, (i)*35, 200, 30);
					text:= '';
					fontDesc:= 'Sans-10:antialias=true';
					flat:= true;
					onClick:= @memButton_press_cb;
				end;
			end;
			//--------------------------------

			// trig buttons ------------------
			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigSin';
				setPosition(5, 340, 55, buttonSize);
				text:= 'sin';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigSinh';
				setPosition(65, 340, 55, buttonSize);
				text:= 'sinh';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigAsin';
				setPosition(125, 340, 55, buttonSize);
				text:= 'asin';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigAsinh';
				setPosition(185, 340, 55, buttonSize);
				text:= 'asinh';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigCos';
				setPosition(5, 380, 55, buttonSize);
				text:= 'cos';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigCosh';
				setPosition(65, 380, 55, buttonSize);
				text:= 'cosh';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigAcos';
				setPosition(125, 380, 55, buttonSize);
				text:= 'acos';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigAcosh';
				setPosition(185, 380, 55, buttonSize);
				text:= 'acosh';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigTan';
				setPosition(5, 420, 55, buttonSize);
				if (settings^.language = 1) then
					text:= 'tan'
				else
					text:= 'tg';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigTanh';
				setPosition(65, 420, 55, buttonSize);
				if (settings^.language = 1) then
					text:= 'tanh'
				else
					text:= 'tgh';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigAtan';
				setPosition(125, 420, 55, buttonSize);
				if (settings^.language = 1) then
					text:= 'atan'
				else
					text:= 'atg';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigAtanh';
				setPosition(185, 420, 55, buttonSize);
				if (settings^.language = 1) then
					text:= 'atanh'
				else
					text:= 'atgh';
				fontDesc:= 'Sans-8:antialias=true';
				onCLick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigCot';
				setPosition(5, 460, 55, buttonSize);
				if (settings^.language = 1) then
					text:= 'cot'
				else
					text:= 'ctg';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigCoth';
				setPosition(65, 460, 55, buttonSize);
				if (settings^.language = 1) then
					text:= 'coth'
				else
					text:= 'ctgh';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigAcot';
				setPosition(125, 460, 55, buttonSize);
				if (settings^.language = 1) then
					text:= 'acot'
				else
					text:= 'actg';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonTrigAcoth';
				setPosition(185, 460, 55, buttonSize);
				if (settings^.language = 1) then
					text:= 'acoth'
				else
					text:= 'actgh';
				fontDesc:= 'Sans-8:antialias=true';
				onClick:= @button_press_cb;
			end;
			//--------------------------------
		end;
	//----------------------------------------------------------------------------------------------

	// buttons callbacks ---------------------------------------------------------------------------
	procedure TmainForm.button_press_cb(sender: Tobject);
		var
			s: string;		// dummy var
			i: integer;		// dummy var

			// used to simulte key press when "=" button is pressed
			shiftState: TshiftState;
			aText: word;
			consumed: boolean;
		begin
			case ((sender as TfpgButton).name) of
				'button0'     : editBox.insertAtCursorPos('0');
				'button1'     : editBox.insertAtCursorPos('1');
				'button2'     : editBox.insertAtCursorPos('2');
				'button3'     : editBox.insertAtCursorPos('3');
				'button4'     : editBox.insertAtCursorPos('4');
				'button5'     : editBox.insertAtCursorPos('5');
				'button6'     : editBox.insertAtCursorPos('6');
				'button7'     : editBox.insertAtCursorPos('7');
				'button8'     : editBox.insertAtCursorPos('8');
				'button9'     : editBox.insertAtCursorPos('9');
				'buttonSpace' : editBox.insertAtCursorPos(' ');
				'buttonLog'   : editBox.insertAtCursorPos('log(');
				'buttonLn'    : editBox.insertAtCursorPos('ln(');
				'buttonPi'    : editBox.insertAtCursorPos(UTF8String(#$CF#$80));
				'buttonE'     : editBox.insertAtCursorPos('e');
				'buttonPower' : editBox.insertAtCursorPos('^');
				'buttonSqrt'  : editBox.insertAtCursorPos(UTF8String(#$E2#$88#$9A));
				'buttonMul'   : editBox.insertAtCursorPos(UTF8String(#$C3#$97));
				'buttonDiv'   : editBox.insertAtCursorPos(UTF8String(#$C3#$B7));
				'buttonAdd'   : editBox.insertAtCursorPos('+');
				'buttonSub'   : editBox.insertAtCursorPos('-');
				'buttonLpar'  : editBox.insertAtCursorPos('(');
				'buttonRpar'  : editBox.insertAtCursorPos(')');
				'buttonPoint' : editBox.insertAtCursorPos('.');
				'buttonClear' : editBox.clear;
				'buttonBcksp' : begin
									s:= editBox.text;
									i:= editBox.getCursorPos;
									
									if ((s[i] = #$C3) or (s[i] = #$CF)) then
										delete(s, i, 2)
									else if (s[i] = #$E2) then
										delete(s, i, 3)
									else
										delete(s, i, 1);
									
									editBox.text:= s;
									editBox.setCursorPos(i - 1);
								end;
				'buttonEqual' : begin
									aText:= 13;
									consumed:= false;
									editBox_on_change_cb(editBox, aText, shiftState, consumed);
								end;
				'buttonMs'	  : begin
									if (width = winWidth) then
										width:= winWidth+215
									else
										width:= winWidth;
										
									updateWindowPosition;
								end;
				'buttonMadd'  : begin
									for i:= 1 to 10 do
										if (i = 10) then begin
											errorLabel.text:= raise_error_gui(settings, 40);
											data^.err:= 0;
											break;
										end else if (memButtons[i].text = '') then begin
												memButtons[i].text:= editBox.text;
												break;
											end;
								end;
				'buttonMsub'  : memButtons[selectedButton].text:= '';
				'buttonMr'    : editBox.insertAtCursorPos(memButtons[selectedButton].text);
				'buttonMc'    : for i:= 1 to 9 do
									memButtons[i].text:= '';
				'buttonTrig'  : begin
									if (height = winHeight) then
										height:= winHeight+160
									else
										height:= winHeight;
									
									updateWindowPosition;
								end;
				'buttonTrigSin'   : editBox.insertAtCursorPos('sin(');
				'buttonTrigSinh'  : editBox.insertAtCursorPos('sinh(');
				'buttonTrigAsin'  : editBox.insertAtCursorPos('asin(');
				'buttonTrigAsinh' : editBox.insertAtCursorPos('asinh(');
				'buttonTrigCos'   : editBox.insertAtCursorPos('cos(');
				'buttonTrigCosh'  : editBox.insertAtCursorPos('cosh(');
				'buttonTrigAcos'  : editBox.insertAtCursorPos('acos(');
				'buttonTrigAcosh' : editBox.insertAtCursorPos('acosh(');
				'buttonTrigTan'   : if (settings^.language = 1) then
										editBox.insertAtCursorPos('tan(')
									else
										editBox.insertAtCursorPos('tg(');
				'buttonTrigTanh'  : if (settings^.language = 1) then
										editBox.insertAtCursorPos('tanh(')
									else
										editBox.insertAtCursorPos('tgh(');
				'buttonTrigAtan'  : if (settings^.language = 1) then
										editBox.insertAtCursorPos('atan(')
									else
										editBox.insertAtCursorPos('atg(');
				'buttonTrigAtanh' : if (settings^.language = 1) then
										editBox.insertAtCursorPos('atanh(')
									else
										editBox.insertAtCursorPos('atgh(');
				'buttonTrigCot'   : if (settings^.language = 1) then
										editBox.insertAtCursorPos('cot(')
									else
										editBox.insertAtCursorPos('ctg(');
				'buttonTrigCoth'  : if (settings^.language = 1) then
										editBox.insertAtCursorPos('coth(')
									else
										editBox.insertAtCursorPos('ctgh(');
				'buttonTrigAcot'  : if (settings^.language = 1) then
										editBox.insertAtCursorPos('acot(')
									else
										editBox.insertAtCursorPos('actg(');
				'buttonTrigAcoth' : if (settings^.language = 1) then
										editBox.insertAtCursorPos('acoth(')
									else
										editBox.insertAtCursorPos('actgh(');
			end;

			editBox.setFocus;
		end;


	procedure TmainForm.memButton_press_cb(sender: Tobject);
		begin
			case ((sender as TfpgButton).name) of
				'memButtons1': selectedButton:= 1;
				'memButtons2': selectedButton:= 2;
				'memButtons3': selectedButton:= 3;
				'memButtons4': selectedButton:= 4;
				'memButtons5': selectedButton:= 5;
				'memButtons6': selectedButton:= 6;
				'memButtons7': selectedButton:= 7;
				'memButtons8': selectedButton:= 8;
				'memButtons9': selectedButton:= 9;
			end;
		end;
	//----------------------------------------------------------------------------------------------

	// menu items callbacks ------------------------------------------------------------------------
	procedure TmainForm.mi_graph_cb(sender: Tobject);
		var
			cl1: fpImage.TfpColor = (red: 0; green: 0; blue: 65535; alpha: 65535);
			cl2: fpImage.TfpColor = (red: 0; green: 65535; blue: 0; alpha: 65535);
			cl3: fpImage.TfpColor = (red: 65535; green: 0; blue: 0; alpha: 65535);

			frm: TgraphForm;
		begin
			new(graphData);
			with graphData^ do begin
				width:= 1800;	// 3x aa
				height:= 1800;	// 3x aa
				grUnits:= 5;

				func1:= '';
				col1:= cl1;
				f1Start:= -5;
				f1End:= 5;

				func2:= '';
				col2:= cl2;
				f2Start:= -5;
				f2End:= 5;

				func3:= '';
				col3:= cl3;
				f3Start:= -5;
				f3End:= 5;

				{$ifdef unix}
					buttonColPath:= '/tmp/calcGraphCol';
				{$endif}
				{$ifdef windows}
					buttonColPath:= GetEnvironmentVariable('TEMP')+'\calcGraphCol';
				{$endif}
			end;
			//------------------------------------------

			frm:= TgraphForm.create(nil);

			try
				frm.showModal;
			finally
				frm.free;
				dispose(graphData);
			end;
		end;


	procedure TmainForm.mi_options_cb(sender: Tobject);
		var
			frm: ToptionsForm;
		begin
			frm:= ToptionsForm.create(nil);

			try
				frm.showModal;
			finally
				frm.free;
			end;
		end;


	procedure TmainForm.mi_quit_cb(sender: Tobject);
		begin
			close;
		end;


	procedure TmainForm.mi_license_cb(sender: Tobject);
		begin
			case (settings^.language) of
				1:
					showMessage(
					'Copyright (C) 2012-2013 Florea Marius Florin'+#10#10+
					'This software is provided ''as-is'', without any express or implied'+#10+
					'warranty.  In no event will the authors be held liable for any damages'+#10+
					'arising from the use of this software.'+#10#10+
					'Permission is granted to anyone to use this software for any purpose,'+#10+
					'including commercial applications, and to alter it and redistribute it'+#10+
					'freely, subject to the following restrictions:'+#10#10+
					'    1. The origin of this software must not be misrepresented; you must not'+#10+
					'        claim that you wrote the original software. If you use this software'+#10+
					'        in a product, an acknowledgment in the product documentation would be'+#10+
					'        appreciated but is not required.'+#10+
					'    2. Altered source versions must be plainly marked as such, and must not be'+#10+
					'        misrepresented as being the original software.'+#10+
					'    3. This notice may not be removed or altered from any source distribution.'+#10#10+
					'Florea Marius Florin, florea.fmf@gmail.com',
					'License');
				2: showMessage(
					'Copyright (C) 2012-2013 Florea Marius Florin'+#10#10+
					'Acest software este furnizat ''ca atare'', fara nici o garantie exprimata'+#10+
					'sau implicita. Sub nici o forma autorii pot fi trasi la raspundere pentru'+#10+
					'orice daune survenite din folosirea acestui software.'+#10#10+
					'Permisiune este acordata pentru oricine sa foloseasca acest software in orice scop,'+#10+
					'inclusiv aplicatii comerciale, si sa il modifice si redistribuie liber'+#10+
					'sub urmatoarele conditii:'+#10#10+
					'    1. Originea acestui software nu trebuie reprezentata gresit; nu trebuie sa'+#10+
					'        pretinzi ca ai scris softwareul original. Daca folosesti acest software'+#10+
					'        intr-un produs, o confirmare in documentatia produsuli ar fi apreciata'+#10+
					'        dar nu este necesara.'+#10+
					'    2. Codurile sursa modificate trebuiesc marcate clar ca atare, si nu trebuiesc'+#10+
					'        sa fie reprezentate ca fiind softwareul original.'+#10+
					'    3. Acest aviz nu poate fi inlaturat sau modificat din orice distribuire de cod.'+#10#10+
					'Florea Marius Florin, florea.fmf@gmail.com',
					'Licenta');
			end;
		end;


	procedure TmainForm.mi_credits_cb(sender: Tobject);
		begin
			case (settings^.language) of
				1: showMessage(
					'Version: '+version+#10#10+
					'Developer: Florea Marius Florin'+#10#10+
					'Special thanks: '+#10+
					'    FPC team - for making the compiler'+#10+
					'    Graeme Geldenhuys <graemeg@gmail.com> - for developing fpGui',
					'Credits');
				2: showMessage(
					'Versiune: '+version+#10#10+
					'Developer: Florea Marius Florin'+#10#10+
					'Multimiri speciale: '+#10+
					'    Echipa FPC - pentru crearea compilatorului'+#10+
					'    Graeme Geldenhuys <graemeg@gmail.com> - pentru dezvoltarea fpGui',
					'Credite');
			end;
		end;
	//----------------------------------------------------------------------------------------------


	// edit box stuff ------------------------------------------------------------------------------
	(* repalces unicode characters with their ascii counterparts *)
	function replace_unicode_chars(s: string): string;
		var
			i: integer;
			d: string;
		begin
			d:= UTF8String(#$C3#$B7);
			while (pos(d, s) > 0) do begin
				i:= pos(d, s);
				s[i]:= '/';
				delete(s, i+1, 1);
			end;

			d:= UTF8String(#$C3#$97);
			while (pos(d, s) > 0) do begin
				i:= pos(d, s);
				s[i]:= '*';
				delete(s, i+1, 1);
			end;

			d:= UTF8String(#$CF#$80);	// pi
			while (pos(d, s) > 0) do begin
				i:= pos(d, s);
				s[i]:= 'p';
				s[i+1]:= 'i';
			end;

			d:= UTF8String(#$E2#$88#$9A);
			while (pos(d, s) > 0) do begin
				i:= pos(d, s);
				insert('sqrt', s, i);
				delete(s, i+4, 3);
			end;

			result:= s;
		end;


	procedure TmainForm.editBox_on_change_cb(sender: Tobject; var aText: word;
		var shiftState: TshiftState; var consumed: boolean);
		var
			s: string;
		begin
			if (aText = 13) then begin	// return key
				// the user wants to see the equation that lead to this result
				if (data^.res = editBox.text) then begin
					editBox.text:= data^.originalInput;
					editBox.setCursorPos(length(data^.originalInput));
				// the user entered a new equation
				end else begin
					s:= replace_unicode_chars(editBox.text);
					data^.input:= s;
					compute_format(settings, data);
				
					if (data^.err = 0) then begin
						// get the original equation
						data^.originalInput:= editBox.text;
					
						editBox.text:= data^.res;
						editBox.setCursorPos(length(data^.res));	// set cursor pos at end of result
						editBox.setFocus;
					
						errorLabel.text:= '';
					end else begin
						errorLabel.text:= raise_error_gui(settings, data^.err);
						data^.err:= 0;
					end;
				end;
			end;
		end;
	//----------------------------------------------------------------------------------------------



	// options form --------------------------------------------------------------------------------
	constructor ToptionsForm.create(aOwner: Tcomponent);
		begin
			inherited create(aOwner);
		end;


	procedure ToptionsForm.afterCreate();
		begin
			name:= 'optionsForm';
			width:= 340;
			height:= 210;
			sizeable:= false;
			windowPosition:= wpScreenCenter;
			if (settings^.language = 1) then
				windowTitle:= 'Options'
			else
				windowTitle:= 'Optiuni';
				
			// startup mode
			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(10, 10, 160, 20);
				if (settings^.language = 1) then
					text:= 'Set startup mode:'
				else
					text:= 'Mod de pornire:';
				fontDesc:= 'Sans-9:antialias=true';
				{$ifdef windows}
					enabled:= false;
				{$endif}
			end;

			rbutton:= TfpgRadioButton.create(self);
			with rbutton do begin
				name:= 'modeRbutton1';
				setPosition(30, 30, 100, 20);
				text:= 'Gui';
				fontDesc:= 'Sans-9:antialias=true';
				groupIndex:= 1;
				if (settings^.mode = 1) then
					checked:= true;
				{$ifdef windows}
					enabled:= false;
				{$endif}
				onClick:= @button_press_cb;
			end;

			rbutton:= TfpgRadioButton.create(self);
			with rbutton do begin
				name:= 'modeRbutton2';
				setPosition(30, 50, 100, 20);
				text:= 'Cli';
				fontDesc:= 'Sans-9:antialias=true';
				groupIndex:= 1;
				if (settings^.mode = 2) then
					checked:= true;
				{$ifdef windows}
					enabled:= false;
				{$endif}
				onClick:= @button_press_cb;
			end;

			// cli mode
			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(10, 70, 160, 20);
				if (settings^.language = 1) then
					text:= 'Set cli mode:'
				else
					text:= 'Cli mod:';
				fontDesc:= 'Sans-9:antialias=true';
				{$ifdef windows}
					enabled:= false;
				{$endif}
			end;

			rbutton:= TfpgRadioButton.create(self);
			with rbutton do begin
				name:= 'cliModeRbutton1';
				setPosition(30, 90, 100, 20);
				if (settings^.language = 1) then
					text:= 'Passive'
				else
					text:= 'Pasiv';
				fontDesc:= 'Sans-9:antialias=true';
				groupIndex:= 2;
				if (settings^.cliMode = 1) then
					checked:= true;
				{$ifdef windows}
					enabled:= false;
				{$endif}
				onClick:= @button_press_cb;
			end;

			rbutton:= TfpgRadioButton.create(self);
			with rbutton do begin
				name:= 'cliModeRbutton2';
				setPosition(30, 110, 100, 20);
				if (settings^.language = 1) then
					text:= 'Interactive'
				else
					text:= 'Interactiv';
				fontDesc:= 'Sans-9:antialias=true';
				groupIndex:= 2;
				if (settings^.cliMode = 2) then
					checked:= true;
				{$ifdef windows}
					enabled:= false;
				{$endif}
				onClick:= @button_press_cb;
			end;

			// eval method
			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(10, 130, 160, 20);
				if (settings^.language = 1) then
					text:= 'Set evaluation method:'
				else
					text:= 'Metoda de evaluare:';
				fontDesc:= 'Sans-9:antialias=true';
			end;
			
			rbutton:= TfpgRadioButton.create(self);
			with rbutton do begin
				name:= 'evalRbutton1';
				setPosition(30, 150, 100, 20);
				text:= 'Infix';
				fontDesc:= 'Sans-9:antialias=true';
				groupIndex:= 3;
				if (settings^.eval = 1) then
					checked:= true;
				onClick:= @button_press_cb;
			end;

			rbutton:= TfpgRadioButton.create(self);
			with rbutton do begin
				name:= 'evalRbutton2';
				setPosition(30, 170, 100, 20);
				text:= 'Postfix';
				fontDesc:= 'Sans-9:antialias=true';
				groupIndex:= 3;
				if (settings^.eval = 2) then
					checked:= true;
				onClick:= @button_press_cb;
			end;

			// set language
			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(170, 10, 160, 20);
				showHint:= true;
				if (settings^.language = 1) then begin
					text:= 'Set language:';
					hint:= 'Requires restart.';
				end else begin
					text:= 'Limba:';
					hint:= 'Necesita restartarea aplicatiei.';
				end;
				fontDesc:= 'Sans-9:antialias=true';
			end;

			rbutton:= TfpgRadioButton.create(self);
			with rbutton do begin
				name:= 'languageRbutton1';
				setPosition(200, 30, 100, 20);
				if (settings^.language = 1) then
					text:= 'English'
				else
					text:= 'Engleza';
				fontDesc:= 'Sans-9:antialias=true';
				groupIndex:= 4;
				if (settings^.language = 1) then
					checked:= true;
				onClick:= @button_press_cb;
			end;

			rbutton:= TfpgRadioButton.create(self);
			with rbutton do begin
				name:= 'languageRbutton2';
				setPosition(200, 50, 100, 20);
				if (settings^.language = 1) then
					text:= 'Romanian'
				else
					text:= 'Romana';
				fontDesc:= 'Sans-9:antialias=true';
				groupIndex:= 4;
				if (settings^.language = 2) then
					checked:= true;
				onClick:= @button_press_cb;
			end;

			// trig units
			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(170, 70, 160, 20);
				if (settings^.language = 1) then
					text:= 'Trig units:'
				else
					text:= 'Unitati trig:';
				fontDesc:= 'Sans-9:antialias=true';
			end;

			rbutton:= TfpgRadioButton.create(self);
			with rbutton do begin
				name:= 'unitsRbutton1';
				setPosition(200, 90, 100, 20);
				if (settings^.language = 1) then
					text:= 'Degrees'
				else
					text:= 'Grade';
				fontDesc:= 'Sans-9:antialias=true';
				groupIndex:= 5;
				if (settings^.trigUnits = 1) then
					checked:= true;
				onClick:= @button_press_cb;
			end;

			rbutton:= TfpgRadioButton.create(self);
			with rbutton do begin
				name:= 'unitsRbutton2';
				setPosition(200, 110, 100, 20);
				if (settings^.language = 1) then
					text:= 'Radians'
				else
					text:= 'Radiani';
				fontDesc:= 'Sans-9:antialias=true';
				groupIndex:= 5;
				if (settings^.trigUnits = 2) then
					checked:= true;
				onClick:= @button_press_cb;
			end;

			// decimals
			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(170, 130, 160, 20);
				if (settings^.language = 1) then
					text:= 'Decimals:'
				else
					text:= 'Zecimale:';
				fontDesc:= 'Sans-9:antialias=true';
			end;

			editBox:= TfpgSpinEdit.create(self);
			with editBox do begin
				name:= 'editBoxInteger';
				setPosition(200, 150, 100, 20);
				fontDesc:= 'Sans-9:antialias=true';
				negativeColor:= TfpgColor($000000);
				maxValue:= 15;
				minValue:= 0;
				increment:= 1;
				value:= settings^.decimals;
				onChange:= @button_press_cb;
			end;

			// set and close buttons
			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonSet';
				setPosition(170, 185, 70, 24);
				if (settings^.language = 1) then
					text:= 'Save'
				else
					text:= 'Salveaza';
				fontDesc:= 'Sans-9:antialias=true';
				onClick:= @button_press_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonQuit';
				setPosition(250, 185, 70, 24);
				if (settings^.language = 1) then
					text:= 'Close'
				else
					text:= 'Inchide';
				fontDesc:= 'Sans-9:antialias=true';
				onClick:= @button_press_cb;
			end;
		end;


	procedure write_settings_to_file();
		var
			f: file of byte;
		begin
			with settings^ do begin
				assign(f, settingsFile);
				rewrite(f);
				write(f, mode, cliMode, eval, decimals, language, trigUnits);
				close(f);
			end;
		end;


	procedure ToptionsForm.button_press_cb(sender: Tobject);
		begin
			if (sender is TfpgButton) then
				case ((sender as TfpgButton).name) of
					'buttonQuit' : close;
					'buttonSet'  : write_settings_to_file;
				end
			else if (sender is TfpgRadioButton) then
				case ((sender as TfpgRadioButton).name) of
					'modeRbutton1'    : settings^.mode:= 1;
					'modeRbutton2'    : settings^.mode:= 2;
					'cliModeRbutton1' : settings^.cliMode:= 1;
					'cliModeRbutton2' : settings^.cliMode:= 2;
					'evalRbutton1'    : settings^.eval:= 1;
					'evalRbutton2'    : settings^.eval:= 2;
					'languageRbutton1': settings^.language:= 1;
					'languageRbutton2': settings^.language:= 2;
					'unitsRbutton1'   : settings^.trigUnits:= 1;
					'unitsRbutton2'   : settings^.trigUnits:= 2;
				end
			else if (sender is TfpgSpinEdit) then begin
				settings^.decimals:= (sender as TfpgSpinEdit).value;
			end;
		end;
	//----------------------------------------------------------------------------------------------



	// graph form ----------------------------------------------------------------------------------
	constructor TgraphForm.create(aOwner: Tcomponent);
		begin
			inherited create(aOwner);
			onPaint:= @form_paint;

			writer:= TfpWriterJPEG.create;
			TfpWriterJPEG(writer).compressionQuality:= 100;
			TfpWriterJPEG(writer).progressiveEncoding:= true;

			img:= TfpMemoryImage.create(25, 25);

			{$warnings off}
				canv:= TfpImageCanvas.create(img);
			{$warnings on}

			register_images;

			draw_graph(settings, data, graphData);
			load_image_cb(nil);
		end;


	destructor TgraphForm.destroy;
		begin
			writer.free;
			canv.free;
			img.free;
			graphImg.free;

			if (fileExists(graphData^.buttonColPath+'1.jpg')) then
				deleteFile(graphData^.buttonColPath+'1.jpg');
			if (fileExists(graphData^.buttonColPath+'2.jpg')) then
				deleteFile(graphData^.buttonColPath+'2.jpg');
			if (fileExists(graphData^.buttonColPath+'3.jpg')) then
				deleteFile(graphData^.buttonColPath+'3.jpg');

			inherited destroy;
		end;


	procedure TgraphForm.afterCreate;
		begin
			name:= 'graphForm';
			width:= 1000;
			height:= 600;
			sizeable:= false;
			windowPosition:= wpScreenCenter;
			if (settings^.language = 1) then
				windowTitle:= 'Graph'
			else
				windowTitle:= 'Grafic';


			// error label
			errorLabel:= TfpgLabel.create(self);
			with errorLabel do begin
				setPosition(610, 0, 390, 20);
				text:= '';
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Some errors are shown here'
				else
					hint:= 'Unele erori sunt afisate aici';
				fontDesc:= 'Sans-10:antialias=true';
			end;

			// 1st line ----------------------------------
			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(610, 30, 190, 20);
				if (settings^.language = 1) then
					text:= 'Functions:'
				else
					text:= 'Functii:';
				fontDesc:= 'Sans-11:antialias=true';
			end;

			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(610, 65, 40, 20);
				text:= 'f(x)=';
				fontDesc:= 'Sans-11:antialias=true';
			end;

			funcInput:= TfpgEdit.create(self);
			with funcInput do begin
				name:= 'funcInput1';
				autoSelect:= false;
				setFocus;
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Enter here the functions'
				else
					hint:= 'Introdu aici functia';
				setPosition(650, 60, 200, 30);
				fontDesc:= 'Sans-14:antialias=true';
				onKeyPress:= @funcInput_on_keypress_cb;
				onChange:= @funcInput_on_change_cb;
			end;

			funcRange:= TfpgSpinEdit.create(self);
			with funcRange do begin
				name:= 'funcStart1';
				setPosition(855, 60, 50, 30);
				fontDesc:= 'Sans-12:antialias=true';
				negativeColor:= TfpgColor($000000);
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Function range'
				else
					hint:= 'Intervalul functiei';
				maxValue:= 50;
				minValue:= -50;
				increment:= 1;
				largeIncrement:= 5;
				value:= -5;
				onChange:= @get_func_range_cb;
				onExit:= @get_func_range_cb;
			end;

			funcRange:= TfpgSpinEdit.create(self);
			with funcRange do begin
				name:= 'funcEnd1';
				setPosition(910, 60, 50, 30);
				fontDesc:= 'Sans-12:antialias=true';
				negativeColor:= TfpgColor($000000);
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Function range'
				else
					hint:= 'Intervalul functiei';
				maxValue:= 50;
				minValue:= -50;
				increment:= 1;
				largeIncrement:= 5;
				value:= 5;
				onChange:= @get_func_range_cb;
				onExit:= @get_func_range_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'funcCol1';
				setPosition(965, 60, 30, 30);
				text:= '';
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Function color'
				else
					hint:= 'Culoarea functiei';
				imageLayout:= ilImageBottom;
				imageMargin:= 0;
				imageSpacing:= 0;
				showImage:= true;
				imageName:= 'graph.button.col1';
				onClick:= @get_func_color_cb;
			end;
			//--------------------------------------------

			// 2nd line ----------------------------------
			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(610, 105, 40, 20);
				text:= 'f(x)=';
				fontDesc:= 'Sans-11:antialias=true';
			end;

			funcInput:= TfpgEdit.create(self);
			with funcInput do begin
				name:= 'funcInput2';
				autoSelect:= false;
				setPosition(650, 100, 200, 30);
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Enter here the functions'
				else
					hint:= 'Introdu aici functia';
				fontDesc:= 'Sans-14:antialias=true';
				onKeyPress:= @funcInput_on_keypress_cb;
				onChange:= @funcInput_on_change_cb;
			end;

			funcRange:= TfpgSpinEdit.create(self);
			with funcRange do begin
				name:= 'funcStart2';
				setPosition(855, 100, 50, 30);
				fontDesc:= 'Sans-12:antialias=true';
				negativeColor:= TfpgColor($000000);
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Function range'
				else
					hint:= 'Intervalul functiei';
				maxValue:= 50;
				minValue:= -50;
				increment:= 1;
				largeIncrement:= 5;
				value:= -5;
				onChange:= @get_func_range_cb;
				onExit:= @get_func_range_cb;
			end;

			funcRange:= TfpgSpinEdit.create(self);
			with funcRange do begin
				name:= 'funcEnd2';
				setPosition(910, 100, 50, 30);
				fontDesc:= 'Sans-12:antialias=true';
				negativeColor:= TfpgColor($000000);
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Function range'
				else
					hint:= 'Intervalul functiei';
				maxValue:= 50;
				minValue:= -50;
				increment:= 1;
				largeIncrement:= 5;
				value:= 5;
				onChange:= @get_func_range_cb;
				onExit:= @get_func_range_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'funcCol2';
				setPosition(965, 100, 30, 30);
				text:= '';
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Function color'
				else
					hint:= 'Culoarea functiei';
				imageLayout:= ilImageBottom;
				imageMargin:= 0;
				imageSpacing:= 0;
				showImage:= true;
				imageName:= 'graph.button.col2';
				onClick:= @get_func_color_cb;
			end;
			//--------------------------------------------

			// 3rd line ----------------------------------
			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(610, 145, 40, 20);
				text:= 'f(x)=';
				fontDesc:= 'Sans-11:antialias=true';
			end;

			funcInput:= TfpgEdit.create(self);
			with funcInput do begin
				name:= 'funcInput3';
				autoSelect:= false;
				setPosition(650, 140, 200, 30);
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Enter here the functions'
				else
					hint:= 'Introdu aici functia';
				fontDesc:= 'Sans-14:antialias=true';
				onKeyPress:= @funcInput_on_keypress_cb;
				onChange:= @funcInput_on_change_cb;
			end;

			funcRange:= TfpgSpinEdit.create(self);
			with funcRange do begin
				name:= 'funcStart3';
				setPosition(855, 140, 50, 30);
				fontDesc:= 'Sans-12:antialias=true';
				negativeColor:= TfpgColor($000000);
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Function range'
				else
					hint:= 'Intervalul functiei';
				maxValue:= 50;
				minValue:= -50;
				increment:= 1;
				largeIncrement:= 5;
				value:= -5;
				onChange:= @get_func_range_cb;
				onExit:= @get_func_range_cb;
			end;

			funcRange:= TfpgSpinEdit.create(self);
			with funcRange do begin
				name:= 'funcEnd3';
				setPosition(910, 140, 50, 30);
				fontDesc:= 'Sans-12:antialias=true';
				negativeColor:= TfpgColor($000000);
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Function range'
				else
					hint:= 'Intervalul functiei';
				maxValue:= 50;
				minValue:= -50;
				increment:= 1;
				largeIncrement:= 5;
				value:= 5;
				onChange:= @get_func_range_cb;
				onExit:= @get_func_range_cb;
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'funcCol3';
				setPosition(965, 140, 30, 30);
				text:= '';
				showHint:= true;
				if (settings^.language = 1) then
					hint:= 'Function color'
				else
					hint:= 'Culoarea functiei';
				imageLayout:= ilImageBottom;
				imageMargin:= 0;
				imageSpacing:= 0;
				showImage:= true;
				imageName:= 'graph.button.col3';
				onClick:= @get_func_color_cb;
			end;
			//--------------------------------------------

			// graph size
			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(610, 200, 190, 20);
				if (settings^.language = 1) then
					text:= 'Graph size:'
				else
					text:= 'Marime grafic:';
				fontDesc:= 'Sans-11:antialias=true';
			end;

			graphSize:= TfpgSpinEdit.create(self);
			with graphSize do begin
				name:= 'graphSize';
				setPosition(610, 230, 60, 30);
				fontDesc:= 'Sans-14:antialias=true';
				negativeColor:= TfpgColor($000000);
				maxValue:= 50;
				minValue:= 5;
				increment:= 1;
				largeIncrement:= 5;
				value:= 5;
				onChange:= @get_func_range_cb;
			end;

			// draw button
			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(610, 290, 100, 30);
				if (settings^.language = 1) then
					text:= 'Draw graph:'
				else
					text:= 'Deseneaza grafic:';
				fontDesc:= 'Sans-11:antialias=true';
			end;

			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonDraw';
				setPosition(610, 320, 80, 24);
				if (settings^.language = 1) then
					text:= 'Draw'
				else
					text:= 'Deseneaza';
				fontDesc:= 'Sans-9:antialias=true';
				onClick:= @button_press_cb;
			end;

			// save image
			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(610, 360, 100, 30);
				if (settings^.language = 1) then
					text:= 'Save:'
				else
					text:= 'Salveaza:';
				fontDesc:= 'Sans-11:antialias=true';
			end;

			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(610, 390, 100, 20);
				if (settings^.language = 1) then
					text:= 'File type:'
				else
					text:= 'Tipul de imagine:';
				fontDesc:= 'Sans-9:antialias=true';
			end;

			imgType:= TfpgRadioButton.create(self);
			with imgType do begin
				name:= 'imgType1';
				setPosition(630, 410, 80, 20);
				text:= '.png';
				checked:= true;
				graphData^.saveImgType:= 1;
				fontDesc:= 'Sans-9:antialias=true'; 
				groupIndex:= 1;
				onClick:= @get_image_data_cb;
			end;

			imgType:= TfpgRadioButton.create(self);
			with imgType do begin
				name:= 'imgType2';
				setPosition(710, 410, 80, 20);
				text:= '.jpg';
				fontDesc:= 'Sans-9:antialias=true';
				groupIndex:= 1;
				onClick:= @get_image_data_cb;
			end;

			imgType:= TfpgRadioButton.create(self);
			with imgType do begin
				name:= 'imgType3';
				setPosition(790, 410, 80, 20);
				text:= '.bmp';
				fontDesc:= 'Sans-9:antialias=true';
				groupIndex:= 1;
				onClick:= @get_image_data_cb;
			end;

			label_:= TfpgLabel.create(self);
			with label_ do begin
				setPosition(610, 430, 100, 20);
				if (settings^.language = 1) then
					text:= 'File size:'
				else
					text:= 'Marimea imaginii:';
				fontDesc:= 'Sans-9:antialias=true';
			end;

			imgSize:= TfpgRadioButton.create(self);
			with imgSize do begin
				name:= 'imgSize1';
				setPosition(630, 450, 80, 20);
				text:= '300x300';
				fontDesc:= 'Sans-9:antialias=true';
				groupIndex:= 2;
				onClick:= @get_image_data_cb;
			end;

			imgSize:= TfpgRadioButton.create(self);
			with imgSize do begin
				name:= 'imgSize2';
				setPosition(710, 450, 80, 20);
				text:= '600x600';
				checked:= true;
				graphData^.saveImgSize:= 600;
				fontdesc:= 'Sans-9:antialias=true';
				groupIndex:= 2;
				onClick:= @get_image_data_cb;
			end;


			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonSave';
				setPosition(610, 480, 80, 24);
				if (settings^.language = 1) then
					text:= 'Save'
				else
					text:= 'Salveaza';
				fontDesc:= 'Sans-9:antialias=true';
				onClick:= @button_press_cb;
			end;

			// close button
			button:= TfpgButton.create(self);
			with button do begin
				name:= 'buttonClose';
				setPosition(920, 576, 80, 24);
				if (settings^.language = 1) then
					text:= 'Close'
				else
					text:= 'Inchidere';
				fontDesc:= 'Sans-9:antialias=true';
				onClick:= @button_press_cb;
			end;
		end;


	procedure TgraphForm.funcInput_on_keypress_cb(sender: Tobject; var aText: word;
					var shiftState: TshiftState; var consumed: boolean);
		begin
			if (aText = 13) then begin
				draw_graph(settings, data, graphData);
				load_image_cb(nil);
			end;
		end;


	procedure TgraphForm.funcInput_on_change_cb(sender: Tobject);
		begin
			with graphData^ do
				case ((sender as TfpgEdit).name) of
					'funcInput1' : func1:= (sender as TfpgEdit).text;
					'funcInput2' : func2:= (sender as TfpgEdit).text;
					'funcInput3' : func3:= (sender as TfpgEdit).text;
				end; 
		end;


	procedure TgraphForm.get_func_range_cb(sender: Tobject);
		var
			edt: TfpgSpinEdit;
		begin
			edt:= TfpgSpinEdit(sender);
			
			if (abs(edt.value) > 50) then
				errorLabel.text:= raise_error_gui(settings, 50)
			else
				errorLabel.text:= '';

			case (edt.name) of
				'funcStart1' : graphData^.f1Start:= edt.value;
				'funcStart2' : graphData^.f2Start:= edt.value;
				'funcStart3' : graphData^.f3Start:= edt.value;
				'funcEnd1'   : graphData^.f1End:= edt.value;
				'funcEnd2'   : graphData^.f2End:= edt.value;
				'funcEnd3'   : graphData^.f3End:= edt.value;
				'graphSize'  : if ((edt.value < 5) or (edt.value > 50)) then
									errorLabel.text:= raise_error_gui(settings, 51)
							   else begin
									errorLabel.text:= '';
									graphData^.grUnits:= edt.value;
							   end;
			end;
		end;


	procedure TgraphForm.get_func_color_cb(sender: Tobject);
		var
			frm: TgraphColorForm;
		begin
			frm:= TgraphColorForm.create(nil);

			case ((sender as TfpgButton).name) of
				'funcCol1' : try
								graphData^.colorToChange:= 1;
								frm.showModal;
							 finally
								frm.free;
								graphData^.colorToChange:= 0;
							 end;
				'funcCol2' : try
								graphData^.colorToChange:= 2;
								frm.showModal;
							 finally
								frm.free;
								graphData^.colorToChange:= 0;
							 end;
				'funcCol3' : try
								graphData^.colorToChange:= 3;
								frm.showModal;
							 finally
								frm.free;
								graphData^.colorToChange:= 0;
							 end;
			end;

			deregister_images;
			update_images;
			register_images;

			// WHAT THE FUCK?
			// workarounds: no idea how they work, but fact is fact
			// without this the button's color won't update properly
			deregister_images;
			update_images;
			register_images;
			{$ifdef windows}
				errorLabel.setFocus;
			{$endif}
		end;


	procedure TgraphForm.button_press_cb(sender: Tobject);
		var
			saveDlg: TfpgFileDialog;
		begin
			if ((sender as TfpgButton).name = 'buttonClose') then
				close
			else if ((sender as TfpgButton).name = 'buttonDraw') then begin
				draw_graph(settings, data, graphData);
				load_image_cb(nil);
			end else if ((sender as TfpgButton).name = 'buttonSave') then begin
				saveDlg:= TfpgFileDialog.create(nil);
				
				try
					if (saveDlg.runSaveFile) then begin
						graphData^.saveImgPath:= saveDlg.fileName;
						save_graph(settings, graphData);
					end;
				finally
					saveDlg.free;
				end;
			end;
		end;


	procedure TgraphForm.get_image_data_cb(sender: Tobject);
		var
			but: TfpgRadioButton;
		begin
			but:= TfpgRadioButton(sender);

			case (but.name) of
				'imgType1' : graphData^.saveImgType:= 1;
				'imgType2' : graphData^.saveImgType:= 2;
				'imgType3' : graphData^.saveImgType:= 3;
				'imgSize1' : graphData^.saveImgSize:= 300;
				'imgSize2' : graphData^.saveImgSize:= 600;
			end;
		end;


	procedure TgraphForm.load_image_cb(sender: Tobject);
		begin
			if (graphImg <> nil) then
				graphImg.free;
			
			graphImg:= loadImage_JPG(settings^.tempDirectory);
			rePaint;
		end;


	procedure TgraphForm.form_paint(sender: Tobject);
		begin
			canvas.drawImage(0, 0, graphImg);
		end;


	procedure TgraphForm.update_images();
		begin
			// create the first image
			with canv do begin
				lockCanvas;
				brush.style:= bsSolid;
				brush.fpColor:= graphData^.col1;
				floodFill(0, 0);
				unlockCanvas;
			end;
			img.saveToFile(graphData^.buttonColPath+'1.jpg', writer);

			// 2nd image
			with canv do begin
				lockCanvas;
				brush.fpColor:= graphData^.col2;
				floodFill(0, 0);
				unlockCanvas;
			end;
			img.saveToFile(graphData^.buttonColPath+'2.jpg', writer);

			// 3rd image
			with canv do begin
				lockCanvas;
				brush.fpColor:=graphData^.col3;
				floodFill(0, 0);
				unlockCanvas;
			end;
			img.saveToFile(graphData^.buttonColPath+'3.jpg', writer);
		end;


	procedure TgraphForm.register_images();
		var
			im: TfpgImage;
		begin
			update_images;

			im:= loadImage_JPG(graphData^.buttonColPath+'1.jpg');
			fpgImages.addImage('graph.button.col1', im);
			
			im:= loadImage_JPG(graphData^.buttonColPath+'2.jpg');
			fpgImages.addImage('graph.button.col2', im);
			
			im:= loadImage_JPG(graphData^.buttonColPath+'3.jpg');
			fpgImages.addImage('graph.button.col3', im);
		end;


	procedure TgraphForm.deregister_images();
		begin
			fpgImages.deleteImage('graph.button.col1', true);
			fpgImages.deleteImage('graph.button.col2', true);
			fpgImages.deleteImage('graph.button.col3', true);
		end;
	//----------------------------------------------------------------------------------------------



	// graph color form ----------------------------------------------------------------------------
	constructor TgraphColorForm.create(aOwner: Tcomponent);
		begin
			inherited create(aOwner);
			FViaRGB:= false;
		end;


	procedure TgraphColorForm.afterCreate();
		begin
			name:= 'graphColorForm';
			width:= 400;
			height:= 400;
			if (settings^.language = 1) then
				windowTitle:= 'Choose color'
			else
				windowTitle:= 'Alege culoarea';
			windowPosition:= wpScreenCenter;
			sizeable:= false;

			// color wheel
			colorWheel:= TfpgColorWheel.create(self);
			with colorWheel do begin
				name:= 'colorWheel';
				setPosition(20, 20, 250, 250);
			end;

			// value bar
			valBar:= TfpgValueBar.create(self);
			with valBar do begin
				name:= 'valueBar';
				setPosition(300, 20, 50, 250);
				onChange:= @color_changed_cb;
			end;

			// link the two components
			colorWheel.valueBar:= valBar;

			// preview
			preview:= TfpgBevel.create(self);
			with preview do begin
				name:= 'preview';
				setPosition(20, 290, 60, 60);
			end;

			// HSV components
			label_:= TfpgLabel.create(self);
			with label_ do begin
				name:= 'labelHue';
				setPosition(100, 295, 30, 20);
				alignment:= taRightJustify;
				fontDesc:= 'Sans-11:antialias=true';
				text:= 'Hue';
			end;

			edH:= TfpgEdit.create(self);
			with edH do begin
				name:= 'editHue';
				setPosition(140, 290, 70, 25);
				text:= '';
				fontdesc:= 'Sans-10:antialias=true';
			end;

			label_:= TfpgLabel.create(self);
			with label_ do begin
				name:= 'labelSaturation';
				setPosition(100, 325, 30, 20);
				alignment:= taRightJustify;
				fontDesc:= 'Sans-11:antialias=true';
				text:= 'Sat';
			end;

			edS:= TfpgEdit.create(self);
			with edS do begin
				name:= 'editSaturation';
				setPosition(140, 320, 70, 25);
				text:= '';
				fontDesc:= 'Sans-10:antialias=true';
			end;

			label_:= TfpgLabel.create(self);
			with label_ do begin
				name:= 'labelValue';
				setPosition(100, 355, 30, 20);
				alignment:= taRightJustify;
				fontDesc:= 'Sans-11:antialias=true';
				text:= 'Val';
			end;

			edV:= TfpgEdit.create(self);
			with edV do begin
				name:= 'editValue';
				setPosition(140, 350, 70, 25);
				text:= '';
				fontDesc:= 'Sans-10:antialias=true';
			end;

			// RGB components
			label_:= TfpgLabel.create(self);
			with label_ do begin
				name:= 'Red';
				setPosition(230, 295, 50, 20);
				alignment:= taRightJustify;
				text:= 'Red';
				fontDesc:= 'Sans-11:antialias=true';
			end;

			edR:= TfpgEdit.create(self);
			with edR do begin
				name:= 'editRed';
				setPosition(290, 290, 70, 25);
				text:= '';
				fontDesc:= 'Sans-10:antialias=true';
				onExit:= @rgb_changed_cb;
			end;

			label_:= TfpgLabel.create(self);
			with label_ do begin
				name:= 'Green';
				setPosition(230, 325, 50, 20);
				alignment:= taRightJustify;
				text:= 'Green';
				fontDesc:= 'Sans-11:antialias=true';
			end;

			edG:= TfpgEdit.create(self);
			with edG do begin
				name:= 'editGreen';
				setPosition(290, 320, 70, 25);
				text:= '';
				fontDesc:= 'Sans-10:antialias=true';
				onExit:= @rgb_changed_cb;
			end;

			label_:= TfpgLabel.create(self);
			with label_ do begin
				name:= 'Blue';
				setPosition(230, 355, 50, 20);
				alignment:= taRightJustify;
				text:= 'Blue';
				fontDesc:= 'Sans-11:antialias=true';
			end;

			edB:= TfpgEdit.create(self);
			with edB do begin
				name:= 'editBlue';
				setPosition(290, 350, 70, 25);
				text:= '';
				fontDesc:= 'Sans-10:antialias=true';
				onExit:= @rgb_changed_cb;
			end;

			// set button
			button:= TfpgButton.create(self);
			with button do begin
				name:= 'setBtn';
				setPosition(220, 376, 80, 24);
				if (settings^.language = 1) then
					text:= 'Set'
				else
					text:= 'Seteaza';
				fontDesc:= 'Sans-9:antialias=true';
				onClick:= @set_color_cb;
			end;

			// quit button
			button:= TfpgButton.create(self);
			with button do begin
				name:= 'quitBtn';
				setPosition(320, 376, 80, 24);
				if (settings^.language = 1) then
					text:= 'Close'
				else
					text:= 'Inchide';
				fontDesc:= 'Sans-9:antialias=true';
				onClick:= @quit;
			end;
		end;


	procedure TgraphColorForm.quit(sender: Tobject);
		begin
			close;
		end;


	procedure TgraphColorForm.color_changed_cb(sender: Tobject);
		begin
			update_HSV_components_cb(nil);
			if (not(FViaRGB)) then
				update_RGB_components_cb(nil);
		end;


	procedure TgraphColorForm.update_HSV_components_cb(sender: Tobject);
		begin
			edH.text:= intToStr(colorWheel.hue);
			edS.text:= formatFloat('0.000', colorWheel.saturation);
			edV.text:= formatFloat('0.000', valBar.value);
			
			preview.backgroundColor:= valBar.selectedColor; 
		end;


	procedure TgraphColorForm.update_RGB_components_cb(sender: Tobject);
		var
			rgb: TfpColor;
			c: TfpgColor;
		begin
			c:= valBar.selectedColor;
			rgb:= fpImage.TfpColor(fpgColorToFPColor(c));

			edR.text:= intToStr(rgb.red);
			edG.text:= intToStr(rgb.green);
			edB.text:= intToStr(rgb.blue);
		end;


	procedure TgraphColorForm.RGB_changed_cb(sender: Tobject);
		var
			rgb: TfpColor;
			c: TfpgColor;
		begin
			FViaRGB:= true;		// prevent recursive updates

			rgb.red:= strToInt(edR.text);
			rgb.green:= strToInt(edG.text);
			rgb.blue:= strToInt(edB.text);
			
			c:= fpColorTofpgColor(fpg_base.TfpColor(rgb));
			colorWheel.setSelectedColor(c);		// this will trigger colorWheel and valueBar onChange event

			FViaRGB:= false;
		end;


	procedure TgraphColorForm.set_color_cb(sender: Tobject);
		var
			rgb: TfpColor;
			c: TfpgColor;
		begin
			c:= valBar.selectedColor;
			rgb:= fpImage.TfpColor(fpgColorToFPColor(c));

			// because in TfpColor max isn't 255 but 65535
			rgb.red:= rgb.red*257;
			rgb.green:= rgb.green*257;
			rgb.blue:= rgb.blue*257;

			case (graphData^.colorToChange) of
				1 : graphData^.col1:= rgb;
				2 : graphData^.col2:= rgb;
				3 : graphData^.col3:= rgb;
			end;
		end;
	//----------------------------------------------------------------------------------------------



	(* gets the settings from the main program *)
	procedure setSettings(sett: pprogramSettings);
		begin
			settings:= sett;
		end;


	(* gets the data from the main program *)
	procedure setData(dat: pcompData);
		begin
			data:= dat;
		end;


	procedure start_gui(settings: pprogramSettings; data: pcompData);
		var
			frm: TmainForm;
		begin
			setSettings(settings);
			setData(data);
		
			fpgApplication.initialize();
			
			fpgStyleManager.setStyle('calcStyle');
			fpgStyle:= fpgStyleManager.style;
			
			frm:= TmainForm.create(fpgApplication);
			try
				frm.show();
				fpgApplication.run();
			finally
				frm.free();
			end;
		end;


end.
