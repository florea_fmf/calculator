{
	 myTypes -- specific datatypes
	 version 1.0, November 17th, 2012

	 Copyright (C) 2012-2013 Florea Marius Florin

	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.

	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
}

(* contains the custom types used in the program *)


{$mode objfpc}
{$h+}
{$smartlink on}


unit myTypes;


interface


	uses
		FPImage;	// for TfpColor


	const
		maxDecimals = 15;	// maximum number of decimals a number is allowed to have
	

	type
		(* global settings of the program *)
		TprogramSettings = record
			(* in what mode the cli was invoked:
			* 1: passive - evaluates the string and quit;
			* 2: interactive - like 'bc' *)
			cliMode: byte;

			(* in what mode the program is:
			* 1 --> gui;
			* 2 --> cli; *)
			mode: byte;

			(* evaluation mode:
			* 1 --> infix;
			* 2 --> postfix; *)
			eval: byte;

			(* how many decimals to print *)
			decimals: byte;		// max maxDecimals

			(* what language is selected 
			* 1 --> english;
			* 2 --> romanian; *)
			language: byte;

			(* tells what the parameter given to the
			* trig functions means:
			* 1 --> degrees (angles)
			* 2 --> radians *)
			trigUnits: byte;

			(* contains the path where the settings
			* file is located *)
			settingsFile: string;

			(* path to the temporary directory. It is used
			* by th graph unit. The graph is drawn on an
			* image and saved in the tempDirectory, from
			* there loaded in displayed in the TgraphForm;
			* when no longer need it, it is deleted *)
			tempDirectory: string;
		end;

		(* contains data required for computation *)
		TcompData = record
			(* the input string to evaluate *)
			input: string;
			
			(* the equation entered by the user, used after the 
			* contentents of 'input' were calculated and the result
			* feeded back into the program, and the user presses the "="
			* button or return, so the program puts into the input box
			* the original equation *)
			originalInput: string;

			(* the result of computation *)
			res: string;

			(* what error occured during evaluation *)
			err: byte;

			(* 1 --> show errors;
			* 2 --> don't display errors *)
			suppressErr: byte;
		end;


		(* contains data required for making graphs *)
		TgraphData = record
			(* width and height of the image in pixels 
			* note: read-only *)
			width, height: word;

			(* graph units, they have no correlation whatsoever with
			* actual measurements units;
			* they are given by the graph size *)
			grUnits: word;

			(* the function to graph
			* note: no more than 3 funcs because otherwise
			* it will look like a rainbow, and like it or
			* not I'm responsible for what I write *)
			func1, func2, func3: string;

			(* the color corresponding to each func *)
			col1, col2, col3: TfpColor;
			
			(* specifies which color is to be changed *)
			colorToChange: byte;

			(* where to start drawing that function *)
			f1Start, f2Start, f3Start: integer;

			(* at what position to end the drawing of that function *)
			f1End, f2End, f3End: integer;

			(* what format to save the image
			* 1 = png
			* 2 = jpg
			* 3 = bmp *)
			saveImgType: word;

			(* what size to save the image in pixels,
			* only one argument required because w = h *)
			saveImgSize: word;

			(* path where to save the image *)
			saveImgPath: string;

			(* path were the image for the change color
			* button is kept;
			* note: the image number and extension
			* must be appended to this path
			* (this string contains only: '/tmp/calcGraphCol') *)
			buttonColPath: string;
		end;

		pprogramSettings = ^TprogramSettings;
		pcompData = ^TcompData;
		pgraphData = ^TgraphData;


implementation


end.
