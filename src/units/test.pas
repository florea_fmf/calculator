{
	 test -- routines for testing the results of the program
	 version 1.0, November 17th, 2012

	 Copyright (C) 2012-2013 Florea Marius Florin

	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.
	
	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
}

(* test procedures for what can be tested *)


{$mode objfpc}
{$h+}
{$smartlink on}


program test;


uses
	math,
	myTypes, calculator;


var
	settings: pprogramSettings;
	data: pcompData;

	passCount, failCount: word;


procedure pass();
	begin
		inc(passCount);
	end;


procedure fail();
	begin
		inc(failCount);
	end;


procedure print_status();
	begin
		writeln('--------------------------------------');
		writeln('Tests status:');
		writeln('	Total tests: ', passCount+failCount);
		writeln('	Tests failed: ', failCount);
		writeln('	Tests passed: ', passCount);
		writeln('--------------------------------------');
	end;


procedure test_substraction();
	var
		a, b: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -10 to 10 do
			for b:= -10 to 10 do begin
				try
					expected:= a-b;
					with data^ do begin
						str(a, ds);
						input:= concat(ds, '-(');
						str(b, ds);
						input:= concat(input, ds, ')');

						compute_format(settings, data);
						input:= '';

						val(res, dr);
						if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
							writeln('>> test_substraction: got: ', res, '; expected: ', expected:0:15,
								'; eval: ', a, '-(', b, ')');
							fail();
						end else
							pass();
					end;
				except
					// continue as nothing happened; like a boss
				end;	// end try
			end;
	end;


procedure test_addition();
	var
		a, b: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -10 to 10 do
			for b:= -10 to 10 do begin
				try
					expected:= a+b;
					with data^ do begin
						str(a, ds);
						input:= concat(ds, '+(');
						str(b, ds);
						input:= concat(input, ds, ')');

						compute_format(settings, data);
						input:= '';

						val(res, dr);
						if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
							writeln('>> test_addition: got: ', res, '; expected: ', expected:0:15,
								'; eval: ', a, '+(', b, ')');
							fail();
						end else
							pass();
					end;
				except
					// continue as nothing happened; like a boss
				end;	// end try
			end;
	end;


procedure test_division();
	var
		a, b: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -10 to 10 do
			for b:= -10 to 10 do begin
				try
					expected:= a/b;
					with data^ do begin
						str(a, ds);
						input:= concat(ds, '/(');
						str(b, ds);
						input:= concat(input, ds, ')');

						compute_format(settings, data);
						input:= '';

						val(res, dr);
						if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
							writeln('>> test_division: got: ', res, '; expected: ', expected:0:15,
								'; eval: ', a, '/(', b, ')');
							fail();
						end else
							pass();
					end;
				except
					// continue as nothing happened; like a boss
				end;	// end try
			end;
	end;


procedure test_multiplication();
	var
		a, b: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -10 to 10 do
			for b:= -10 to 10 do begin
				try
					expected:= a*b;
					with data^ do begin
						str(a, ds);
						input:= concat(ds, '*(');
						str(b, ds);
						input:= concat(input, ds, ')');

						compute_format(settings, data);
						input:= '';

						val(res, dr);
						if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
							writeln('>> test_multiplication: got: ', res, '; expected: ', expected:0:15,
								'; eval: ', a, '*(', b, ')');
							fail();
						end else
							pass();
					end;
				except
					// continue as nothing happened; like a boss
				end;	// end try
			end;
	end;


procedure test_sqrt();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -1000 to 1000 do
			try
				expected:= sqrt(a);
				with data^ do begin
					str(a, ds);
					input:= concat('sqrt(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_sqrt: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'sqrt(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_log();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -1000 to 1000 do
			try
				expected:= log10(a);
				with data^ do begin
					str(a, ds);
					input:= concat('log(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_log: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'log(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_ln();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -1000 to 1000 do
			try
				expected:= ln(a);
				with data^ do begin
					str(a, ds);
					input:= concat('ln(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_ln: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'ln(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_abs();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -1000 to 1000 do
			try
				expected:= abs(a);
				with data^ do begin
					str(a, ds);
					input:= concat('abs(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_abs: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'abs(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_sin();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -1000 to 1000 do
			try
				expected:= sin(a);
				with data^ do begin
					str(a, ds);
					input:= concat('sin(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_sin: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'sin(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_sinh();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -10 to 10 do
			try
				expected:= sinh(a);
				with data^ do begin
					str(a, ds);
					input:= concat('sinh(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_sinh: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'sinh(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_asin();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -1000 to 1000 do
			try
				expected:= arcsin(a);
				with data^ do begin
					str(a, ds);
					input:= concat('asin(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_asin: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'asin(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_asinh();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -1000 to 1000 do
			try
				expected:= arsinh(a);
				with data^ do begin
					str(a, ds);
					input:= concat('asinh(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_asinh: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'asinh(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_cos();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -1000 to 1000 do
			try
				expected:= cos(a);
				with data^ do begin
					str(a, ds);
					input:= concat('cos(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_cos: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'cos(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_cosh();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -10 to 10 do
			try
				expected:= cosh(a);
				with data^ do begin
					str(a, ds);
					input:= concat('cosh(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_cosh: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'cosh(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_acos();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -1000 to 1000 do
			try
				expected:= arccos(a);
				with data^ do begin
					str(a, ds);
					input:= concat('acos(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_acos: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'acos(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_acosh();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -1000 to 1000 do
			try
				expected:= arcosh(a);
				with data^ do begin
					str(a, ds);
					input:= concat('acosh(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_acosh: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'acosh(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_tan();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -1000 to 1000 do
			try
				expected:= tan(a);
				with data^ do begin
					str(a, ds);
					input:= concat('tan(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_tan: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'tan(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_tanh();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -10 to 10 do
			try
				expected:= tanh(a);
				with data^ do begin
					str(a, ds);
					input:= concat('tanh(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_tanh: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'tanh(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_atan();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -1000 to 1000 do
			try
				expected:= arctan(a);
				with data^ do begin
					str(a, ds);
					input:= concat('atan(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_atan: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'atan(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_atanh();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -1000 to 1000 do
			try
				expected:= artanh(a);
				with data^ do begin
					str(a, ds);
					input:= concat('atanh(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_atanh: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'atanh(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


procedure test_cot();
	var
		a: integer;
		ds: string;
		expected, dr: double;
	begin
		expected:= 0.0;
		for a:= -1000 to 1000 do
			try
				expected:= cot(a);
				with data^ do begin
					str(a, ds);
					input:= concat('cot(', ds, ')');

					compute_format(settings, data);
					input:= '';

					val(res, dr);
					if ((abs(expected-dr) > 0.0000001) and (err = 0)) then begin
						writeln('>> test_cot: got: ', res, '; expected: ', expected:0:15,
							'; eval: ', 'cot(', a, ')');
						fail();
					end else
						pass();
				end;
			except
				// continue as nothing happened; like a boss
			end;	// end try
	end;


(* evaluates data^.input and compares the result to expected string and if that expression
* should raise an error, and what error code it should give 
* note: if the expression should raise an error and data^.err = expected_err
* then the test is marked as passed *)
procedure test(expected: string; expected_err: byte);
	var
		dummy: string;
	begin
		dummy:= data^.input;

		compute_format(settings, data);
		if ((data^.res = expected) and (data^.err = expected_err)) then begin
			data^.err:= 0;
			pass()
		end else if ((expected = '') and (data^.err = expected_err)) then begin
			data^.err:= 0;
			pass()
		end else begin
			writeln('>> test: got: ', data^.res, '; expected: ', expected, '; eval: ', dummy);
			writeln('>> got error: ', data^.err, '; expected error: ', expected_err);
			writeln();
			data^.err:= 0;
			fail();
		end;
	end;

	
begin
	new(settings);
	new(data);

	// test functions with infix notation
	with settings^ do begin
		mode:= 2;
		eval:= 1;
		decimals:= 15;
		language:= 1;
		trigUnits:= 2;
	end;

	// functions bulk check-------------------------------------------------------------------------
	test_substraction();
	test_addition();
	test_division();
	test_multiplication();

	test_sqrt();
	test_abs();

	test_log();
	test_ln();

	test_sin();
	test_sinh();
	test_asin();
	test_asinh();

	test_cos();
	test_cosh();
	test_acos();
	test_acosh();

	test_tan();
	test_tanh();
	test_atan();
	test_atanh();

	test_cot();
	// note: the rest of cot functions are useless to test because
	// those functions are defnined by me, so they will always have
	// the same output

	//----------------------------------------------------------------------------------------------

	// test some (more real world like) funcs ------------------------------------------------------
	with data^ do begin
		err:= 0;
	
		input:= '1';
		test('1', 0);

		input:= '-1';
		test('-1', 0);

		input:= '2*10^3';
		test('2000', 0);

		input:= '2*10^(-3)';
		test('0.002', 0);

		input:= 'pi';
		test('3.14159265358979', 0);

		input:= 'e';
		test('2.71828182845905', 0);

		input:= '0+0';
		test('0', 0);

		input:= '1+1';
		test('2', 0);

		input:= '1+4';
		test('5', 0);

		input:= '40000+0.001';
		test('40000.001', 0);

		input:= '0.001+40000';
		test('40000.001', 0);

		input:= '2*3';
		test('6', 0);

		input:= '-2*3';
		test('-6', 0);

		input:= '2*(-3)';
		test('-6', 0);

		input:= '-2*(-3)';
		test('6', 0);

		input:= '6/3';
		test('2', 0);

		input:= '1/2';
		test('0.5', 0);
		
		input:= '3/0';
		test('', 3);

		input:= '-6/3';
		test('-2', 0);

		input:= '6/(-3)';
		test('-2', 0);

		input:= '-6/(-3)';
		test('2', 0);

		input:= '(-3)/(-6)';
		test('0.5', 0);

		input:= '2/2';
		test('1', 0);

		input:= '1203/1';
		test('1203', 0);

		input:= '1/4';
		test('0.25', 0);

		input:= '1/3';
		test('0.333333333333333', 0);

		input:= '2/3';
		test('0.666666666666667', 0);

		input:= '0/0';
		test('', 3);

		// order of operations
		input:= '1-0.9-0.1';
		test('-0', 0);

		input:= '1+2*3';
		test('7', 0);

		input:= '1+(2*3)';
		test('7', 0);

		input:= '(1+2)*3';
		test('9', 0);

		input:= '(1+2*3)';
		test('7', 0);

		input:= '4/2*(1+1)';
		test('4', 0);

		// power (rangers)
		input:= '2^2';
		test('4', 0);

		input:= '2^2^2';
		test('16', 0);
		
		input:= '2^3^2';
		test('512', 0);

		input:= '2^3';
		test('8', 0);

		input:= '2^10';
		test('1024', 0);

		input:= '(1+2)^2';
		test('9', 0);

		input:= 'abs(1-2)^2';
		test('1', 0);

		input:= '0^0';
		test('1', 0);

		input:= '0^0.5';
		test('0', 0);

		input:= '2^0';
		test('1', 0);

		input:= '2^1';
		test('2', 0);

		input:= '2^(-1)';
		test('0.5', 0);

		input:= '2^';
		test('',  2);

		input:= '-10^2';
		test('-100', 0);

		input:= '(-10)^2';
		test('100', 0);

		input:= '-(10^2)';
		test('-100', 0);

		input:= '4^3^2';
		test('262144', 0);

		input:= '4^(3^2)';
		test('262144', 0);

		input:= '(4^3)^2';
		test('4096', 0);
		
		input:= '4^0.5';
		test('2', 0);

		input:= '2^0.5';
		test('1.4142135623731', 0);
		
		input:= '(-8)^(1/3)';
		test('', 6);
		
		// others
		input:= 'sqrt(4)';
		test('2', 0);

		input:= 'sqrt(4)-2';
		test('0', 0);

		input:= 'sqrt(2+2)';
		test('2', 0);

		input:= '3*sqrt(4)';
		test('6', 0);

		input:= 'log(0)';
		test('', 8);

		input:= 'log(1)';
		test('0', 0);

		input:= 'log(2)';
		test('0.301029995663981', 0);

		input:= 'log(10)';
		test('1', 0);

		input:= '2*log(2)';
		test('0.602059991327963', 0);

		input:= 'ln(0)';
		test('', 8);

		input:= 'ln(1)';
		test('0', 0);

		input:= 'ln(2)';
		test('0.693147180559945', 0);

		input:= 'ln(e)';
		test('1', 0);

		input:= '2*ln(2)';
		test('1.38629436111989', 0);

		input:= 'sin(45)-1/sqrt(2)';
		test('0.143796743347571', 0);

		input:= 'sin(20)+sin(-20)';
		test('0', 0);

		input:= 'cos(45)-1/sqrt(2)';
		test('-0.181784792368818', 0);

		input:= 'cos(20)+cos(-20)';
		test('0.816164123626784', 0);

		input:= 'tan(10)-sin(10)/cos(10)';
		test('0', 0);

		input:= 'tan(pi/2)';
		test('', 10);
	end;
	//----------------------------------------------------------------------------------------------

	print_status();

	dispose(settings);
	dispose(data);
end.