{
	 graph -- part of the program containing the graph
	 version 1.0, November 17th, 2012

	 Copyright (C) 2012-2013 Florea Marius Florin

	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.

	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
}

(* contains routines for making graphs *)


{$mode objfpc}
{$h+}
{$smartlink on}


unit graph;


interface


	uses
		myTypes, extinterpolation;

	(* draw the graph of whatever is in graphData to 
	* a jpeg file in the settings^.tempDirectory *)
	procedure draw_graph(settings: pprogramSettings; data: pcompData; graphData: pgraphData);
	
	(* saves the current graph to file (type and size choosen by user)
	* in a directory choosen by user *)
	procedure save_graph(settings: pprogramSettings; graphData: pgraphData);


implementation


	uses
		classes, sysutils,
		fpImage, fpCanvas, fpImgCanv,
		fpWriteJPEG, fpWritePNG, fpWriteBMP,
		ftFont,
		error, calculator;


	type
		TcubicInterp = class(TCubicInterpolation)
			public
				procedure initialize(aimage: TfpCustomImage; acanvas: TfpCustomCanvas); override;
			end;


	var
		// some colors
		clWhite: TfpColor = (red: 65535; green: 65535; blue: 65535; alpha: 65535);
		clBlack: TfpColor = (red: 0; green: 0; blue: 0; alpha: 65535);
		clPalidBlue: TfpColor = (red: 50000; green: 50000; blue: 65535; alpha: 65535);


	procedure TcubicInterp.initialize(aimage: TfpCustomImage; acanvas: TfpCustomCanvas);
		begin
			inherited initialize(aimage, acanvas);
		end;


	(* replaces the 'x' from 'func' with the 'x' (real) value
	* eg: func = sin(x), x = 2.6 --> result = sin(2.6) *)
	function replace_x_with_value(func: string; x: double): string;
		var
			i: integer;
			d, t: string;
		begin
			while (pos('x', func) > 0) do begin
				i:= pos('x', func);
				str(abs(x):0:9, d);
				
				t:= copy(func, 1, i-1);
				if (x >= 0.0) then
					t:= concat(t, d, copy(func, i+1, length(func)))
				else
					t:= concat(t, '(-', d, ')', copy(func, i+1, length(func)));
					
				func:= t;
			end;
			
			result:= func;
		end;

	
	procedure draw_func(funcNr: integer; var canvas: TfpCustomCanvas; gu: real;
		settings: pprogramSettings; data: pcompData; graphData: pgraphData);
		var
			func: string;
			start, end_, j: integer;
			col: TfpColor;
			w, h: word;
			x, y, i: real;
			firstRun, move: boolean;
		begin
			w:= graphData^.width;
			h:= graphData^.height;
			
			case (funcNr) of
				1 : begin
					func:= graphData^.func1;
					start:= graphData^.f1Start;
					end_:= graphData^.f1End;
					col:= graphData^.col1;
				end;
				2 : begin
					func:= graphData^.func2;
					start:= graphData^.f2Start;
					end_:= graphData^.f2End;
					col:= graphData^.col2;
				end;
				3 : begin
					func:= graphData^.func3;
					start:= graphData^.f3Start;
					end_:= graphData^.f3End;
					col:= graphData^.col3;
				end;
			end;
			
			firstRun:= true;
			canvas.pen.fpColor:= col;
			canvas.pen.width:= 6;

			i:= start;
			move:= false;
			while (i <= end_) do begin
				// get the first position where the drawing should begin
				if (firstRun) then begin
					data^.input:= replace_x_with_value(func, i);
					compute_format(settings, data);
					if (data^.err = 0) then
						val(data^.res, y)
					else begin
						// what error to raise here?
						data^.err:= 0;
					end;

					canvas.moveTo(round((w/2)+(i*gu)), round((h/2)-(y*gu)));
					firstRun:= false;
				end;

				// compute intermediate results for better accuracy
				for j:= 1 to 10 do begin
					x:= i+(j/100);

					data^.input:= replace_x_with_value(func, x);
					compute_format(settings, data);
						
					if (data^.err = 0) then
						val(data^.res, y)
					else begin
						//writeln('--> error in computation');
						// there is an error and in like 99% of the
						// cases we should not draw it, so we
						// 'move' after this position
						move:= true;
						data^.err:= 0;
						continue;
					end;

					if (move) then begin
						canvas.moveTo(round((w/2)+(x*gu)), round((h/2)-(y*gu)));
						move:= false;
					end;
						
					canvas.lineTo(round((w/2)+(x*gu)), round((h/2)-(y*gu)));
				end;	// end for

				i:= i+0.1;
			end;	// end while}
		end;
		
	(* draw the function name, namely that thing on the top-left conrner of window
	* describing what each line represents *)
	procedure draw_func_name(funcNr: integer; var canvas: TfpCustomCanvas; graphData: pgraphData);
		var
			func: string;
			col: TfpColor;
			offset: integer;	// between functions there must be a 35 pixels difference 
		begin
			case (funcNr) of
				1 : begin
					func:= graphData^.func1;
					col:= graphData^.col1;
					offset:= 22;
				end;
				2 : begin
					func:= graphData^.func2;
					col:= graphData^.col2;
					offset:= 57;
				end;
				3 : begin
					func:= graphData^.func3;
					col:= graphData^.col3;
					offset:= 92;
				end;
			end;
				
			canvas.pen.fpColor:= col;
			canvas.pen.width:= 15;
			canvas.moveTo(0, offset);
			canvas.lineTo(55, offset);
			canvas.textOut(60, offset+13, 'f(x)='+func);
		end;
	
	
	procedure draw_funcs(var canvas: TfpCustomCanvas; gu: real;  settings: pprogramSettings;
		data: pcompData; graphData: pgraphData);
		begin	
			if (graphData^.func1 <> '') then begin
				draw_func(1, canvas, gu, settings, data, graphData);
				draw_func_name(1, canvas, graphData);
			end;

			if (graphData^.func2 <> '') then begin
				draw_func(2, canvas, gu, settings, data, graphData);
				draw_func_name(2, canvas, graphData);
			end;
			
			if (graphData^.func3 <> '') then begin
				draw_func(3, canvas, gu, settings, data, graphData);
				draw_func_name(3, canvas, graphData);
			end;
		end;


	(* draws the numbers and blue lines on the image 
	* canvas = the canvas with the picture
	* w = width of the picture
	* h = height of the picture
	* i = current iteration in loop
	* x = current position in pixels of the current iteration *)
	procedure draw_x_axis(var canvas: TfpCustomCanvas; w, h: word; i: integer; x: double);
		var
			s: string;
		begin
			// from origin to +inf
			with canvas do begin
				// draw the math like background
				pen.fpColor:= clPalidBlue;
				moveTo(round((w/2)+x), 0);
				lineTo(round((w/2)+x), h);

				// draw the number
				str(i, s);
				textOut(round((w/2)+x), round((h/2)-10), s);

				// put the dot on the axis
				pen.fpColor:= clBlack;
				moveTo(round((w/2)+x), round((h/2)-10));
				lineTo(round((w/2)+x), round((h/2)+10));
			end;

			// from origin to -inf
			with canvas do begin
				pen.fpColor:= clPalidBlue;
				moveTo(round((w/2)-x), 0);
				lineTo(round((w/2)-x), h);

				str(i, s);
				s:= concat('-', s);
				textOut(round((w/2)-x), round((h/2)-10), s);

				pen.fpColor:= clBlack;
				moveTo(round((w/2)-x), round((h/2)-10));
				lineTo(round((w/2)-x), round((h/2)+10));
			end;
		end;


	(* draw the numbers and blue lines on the image *)
	procedure draw_y_axis(var canvas: TfpCustomCanvas; w, h: word; i: integer; x: double);
		var
			s: string;
		begin
			// from origin to +inf
			with canvas do begin
				pen.fpColor:= clPalidBlue;
				moveTo(0, round((h/2)-x));
				lineTo(w, round((h/2)-x));

				str(i, s);
				textOut(round((w/2)+10), round((h/2)-x), s);

				pen.fpColor:= clBlack;
				moveTo(round((w/2)-10), round((h/2)-x));
				lineTo(round((w/2)+10), round((h/2)-x));
			end;

			// from origin to -inf
			with canvas do begin
				pen.fpColor:= clPalidBlue;
				moveTo(0, round((h/2)+x));
				lineTo(w, round((h/2)+x));

				str(i, s);
				s:= concat('-', s);
				textOut(round((w/2)+10), round((h/2)+x), s);

				pen.fpColor:= clBlack;
				moveTo(round((w/2)-10), round((h/2)+x));
				lineTo(round((w/2)+10), round((h/2)+x));
			end;
		end;


	procedure draw_graph(settings: pprogramSettings; data: pcompData; graphData: pgraphData);
		var
			i: integer;
			gu, x: real;	// graph unit
			canvas: TfpCustomCanvas;		// what we use to draw on the high res image
			image: TfpCustomImage;			// the high res image we draw on
			writer: TfpCustomImageWriter;	// what we use to save the image on disk
			ttfFont: TfreeTypeFont;	// fucking name clashes (>.<)

			img: TfpCustomImage;	// the image saved to disk
			canv: TfpCustomCanvas;	// canvas for the above img
			rect: Trect = (left: 0; top: 0; right: 600; bottom: 600);	// used to get image after resize
			interp: TcubicInterp;
		begin
			// since w = h doesn't matter which is used
			gu:= (graphData^.width/2)/graphData^.grUnits;

			// initialize free type font manager
			ftFont.initEngine;
			{$ifdef unix}
				// TO DO: make this distro independent
				if (fileExists('/usr/share/fonts/TTF')) then
					fontMgr.searchPath:= '/usr/share/fonts/TTF/'
				else if (fileExists('/usr/share/fonts/truetype')) then
					fontMgr.searchPath:= '/usr/share/fonts/truetype/ttf-dejavu/';
			{$endif}
			{$ifdef windows}
				fontMgr.searchPath:= 'C:\Windows\Fonts\';
			{$endif}
			ttfFont:= TfreeTypeFont.create;

			// init the writer
			writer:= TfpWriterJPEG.create;
			TfpWriterJPEG(writer).compressionQuality:= 100;	// resulting jpeg quality
			TfpWriterJPEG(writer).progressiveEncoding:= true;	{ don't know what it actually does,
																except that it makes the files smaller }

			// init the image
			image:= TfpMemoryImage.create(graphData^.width, graphData^.height);
			img:= TfpMemoryImage.create(600, 600);

			// init the canvas
			{$warnings off}
				canvas:= TfpImageCanvas.create(image);
				canv:= TfpImageCanvas.create(img);
			{$warnings on}
			
			// init interpolation
			interp:= TcubicInterp.create;
			interp.initialize(image, canvas);

			canvas.lockCanvas;

			with canvas do begin
				pen.mode:= pmCopy;
				pen.style:= psSolid;
				pen.width:= 5;
				
				canvas.interpolation:= interp;

				brush.style:= bsSolid;
				brush.fpColor:= clWhite;

				font:= ttfFont;
				{$ifdef unix}
					font.name:= 'DejaVuSans';	// TO DO: make this more ..uhm.. universal
				{$endif}
				{$ifdef windows}
					font.name:= 'Arial';
				{$endif}
				font.size:= 30;
			end;

			// make bg white
			canvas.floodFill(0, 0);

			// draw numbers, and blue lines on the axix
			x:= gu;
			for i:= 1 to graphData^.grUnits do begin
				case (graphData^.grUnits) of
					0..10   : begin
								draw_x_axis(canvas, graphData^.width, graphData^.height, i, x);
								draw_y_axis(canvas, graphData^.width, graphData^.height, i, x);
							 end;
					11..20 : if (i mod 2 = 0) then begin
								draw_x_axis(canvas, graphData^.width, graphData^.height, i, x);
								draw_y_axis(canvas, graphData^.width, graphData^.height, i, x);
							 end;
					21..30 : if (i mod 4 = 0) then begin
								draw_x_axis(canvas, graphData^.width, graphData^.height, i, x);
								draw_y_axis(canvas, graphData^.width, graphData^.height, i, x);
							 end;
					31..40 : if (i mod 6 = 0) then begin
								draw_x_axis(canvas, graphData^.width, graphData^.height, i, x);
								draw_y_axis(canvas, graphData^.width, graphData^.height, i, x);
							 end;
					41..50 : if (i mod 8 = 0) then begin
								draw_x_axis(canvas, graphData^.width, graphData^.height, i, x);
								draw_y_axis(canvas, graphData^.width, graphData^.height, i, x);
							 end;
				end;	// end case
				
				x:= x+gu;
			end;	// end for

			// draw axes -------------------------------------
			with canvas do begin
				pen.fpColor:= clBlack;

				// x axis
				moveTo(0, round(graphData^.height/2));
				lineTo(graphData^.width, round(graphData^.height/2));

				// put the name of the axis
				textOut(graphData^.width-30, round((graphData^.height/2)+35), 'x');

				//y axis
				moveTo(round(graphData^.width/2), 0);
				lineTo(round(graphData^.width/2), graphData^.height);
				
				textOut(round((graphData^.width/2)-35), 30, 'y');
			end;
			//------------------------------------------------


			// draw the funcs --------------------------------
			draw_funcs(canvas, gu, settings, data, graphData);
			//------------------------------------------------
			canvas.stretchDraw(0, 0, 600, 600, image);

			canvas.unlockCanvas;

			{ we use the oldest trick to make the picture antialised
			* that is: draw the actual picture at a much higher
			* resolution then the one we will use to display
			* (here the image is displayed at 600x600px, yet
			* the image is drawn at a resolution of 1800x1800px
			* ~ 3x aa), then resize the picture }
			canv.lockCanvas;
			canv.copyRect(0, 0, canvas, rect);
			canv.unlockCanvas;

			img.saveToFile(settings^.tempDirectory, writer);


			// clean up
			canvas.free;
			canv.free;
			interp.free;
			image.free;
			img.free;
			ttfFont.free;
			writer.free;
		end;
		

	procedure save_graph(settings: pprogramSettings; graphData: pgraphData);
		var
			image, img: TfpCustomImage;
			canvas, canv: TfpCustomCanvas;
			writer: TfpCustomImageWriter;
			rect: Trect;
			extension, s: string;
			interp: TcubicInterp;
		begin
			// init stuff
			image:= TfpMemoryImage.create(0, 0);
			image.loadFromFile(settings^.tempDirectory);
			if (graphData^.saveImgSize <> 600) then
				img:= TfpMemoryImage.create(graphData^.saveImgSize, graphData^.saveImgSize);

			{$warnings off}
			canvas:= TfpImageCanvas.create(image);
			if (graphData^.saveImgSize <> 600) then begin
				canv:= TfpImageCanvas.create(img);
				
				interp:= TcubicInterp.create;
				interp.initialize(image, canv);
			end;
			{$warnings on}

			case (graphData^.saveImgType) of
				1 : begin
						writer:= TfpWriterPNG.create;
						TfpWriterPNG(writer).indexed:= false;
						extension:= '.png';
					end;
				2 : begin
						writer:= TfpWriterJPEG.create;
						TfpWriterJPEG(writer).compressionQuality:= 100;
						TfpWriterJPEG(writer).progressiveEncoding:= true;
						extension:= '.jpg';
					end;
				3 : begin
						writer:= TfpWriterBMP.create;
						extension:= '.bmp';
					end;
			end;

			
			// transform image if necessary
			with graphData^ do begin
				if (saveImgSize <> 600) then begin
					rect.left:= 0;
					rect.top:= 0;
					rect.right:= saveImgSize;
					rect.bottom:= saveImgSize;

					canv.lockCanvas;
					canv.copyRect(0, 0, canvas, rect);
					canv.interpolation:= interp;
					canv.stretchDraw(0, 0, saveImgSize, saveImgSize, image);
					canv.unlockCanvas;
				end;
			end;


			// check to see if file has extension
			s:= copy(graphData^.saveImgPath, length(graphData^.saveImgPath)-3,
				length(graphData^.saveImgPath));
			case (s) of
				'.png' : if (graphData^.saveImgType = 1) then
							extension:= ''
						 else begin
							deleteFile(graphData^.saveImgPath);
							delete(graphData^.saveImgPath, length(graphData^.saveImgPath)-3,
								length(graphData^.saveImgPath));
						 end;
				'.jpg' : if (graphData^.saveImgType = 2) then
							extension:= ''
						 else begin
							deleteFile(graphData^.saveImgPath);
							delete(graphData^.saveImgPath, length(graphData^.saveImgPath)-3,
								length(graphData^.saveImgPath));
						 end;
				'.bmp' : if (graphData^.saveImgType = 3) then
							extension:= ''
						 else begin
							deleteFile(graphData^.saveImgPath);
							delete(graphData^.saveImgPath, length(graphData^.saveImgPath)-3,
								length(graphData^.saveImgPath));
						 end;
			end;

			// write file
			if (graphData^.saveImgSize = 600) then
				image.saveToFile(graphData^.saveImgPath+extension, writer)
			else
				img.saveToFile(graphData^.saveImgPath+extension, writer);


			// free resources
			canvas.free;
			if (graphData^.saveImgSize <> 600) then begin
				canv.free;
				img.free;
				interp.free;
			end;
			image.free;
			writer.free;
		end;

end.
