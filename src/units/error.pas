{
	 error -- (very) basic error handling
	 version 1.0, November 17th, 2012

	 Copyright (C) 2012-2013 Florea Marius Florin

	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.

	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
}

(* error handling routines *)


{$mode objfpc}
{$h+}
{$smartlink on}


unit error;


interface


	uses
		myTypes;


	(* halts the program and display what error occured 
	* NOTE: the program is halted by the calling procedure *)
	(* returns the explanation of the error to be displayed
	* in the table under the input field *)
	function raise_error_gui(settings: pprogramSettings; err: byte): string;
	procedure raise_error_cli(settings: pprogramSettings; err: byte);
	procedure raise_error_startup(settings: pprogramSettings; err: byte; additionalArgs: string);


implementation


	(* returns an explanation of the error, or a wanna be explanation *)
	function return_error_extended(settings: pprogramSettings; err: byte): string;
		begin
			case settings^.language of
				1 : case err of
						255 : result:= 'dafuq?';
						// computation errors
						1 : result:= 'Internal error: Stack is full';
						2 : result:= 'Internal error: Stack is empty';
						3 : result:= 'Division by 0';
						4 : result:= 'Malformed expression';
						5 : result:= 'The power of 0 is undefined for a negative exponent';
						6 : result:= 'A negative number raised to a real power has as a result'+
							' a complex number';
						7 : result:= 'Square root of a negative numbers has as a result'+
							' a complex number';
						8 : result:= 'Logarithm of 0 is undefined';
						9 : result:= 'Logarithm of a negative number has as a result'+
							' a complex number';
						10: result:= 'Tangent is undefined for angles that are multiples'+
							' of pi/2 (90 degrees)';
						11: result:= 'Cotangent is undefined for angles that are multiples'+
							' of pi (180 degrees)';
						12: result:= 'Arcsin works only in the range [-1, 1] (radians)';
						13: result:= 'Arccos works only in the range [-1, 1] (radians)';
						14: result:= 'Cotanh of 0 is undefined';
						15: result:= 'Arccsoh works only in the interval (-∞, -1) ∩ [1, +∞)';
						16: result:= 'Arctanh works only in the range (-1, 1) (radians)';
						17: result:= 'Arccoth works only in the interval (-∞, -1) ∩ (1, +∞)';
						// startup errors
						30: result:= 'Unrecognised evaluation method: ';
						31: result:= 'Unrecognised number: ';
						32: result:= 'Entered number is not in range [0..15]: ';
						33: result:= 'Unrecognised language: ';
						34: result:= 'Unrecognised unit: ';
						35: result:= 'Unrecognised setting: ';
						// gui only erros
						40: result:= 'Memory is full';
						// graph errors
						50: result:= 'Function range must be between -50 and 50.';
						51: result:= 'Graph size must be between 5 and 50.';
					end;	// case 'err' end
				2 : case err of
						255 : result:= 'dafuq?';
						1 : result:= 'Eroare interna: Stiva este plina';
						2 : result:= 'Eroare interna: Stiva este goala';
						3 : result:= 'Impartire la 0';
						4 : result:= 'Eroare in scrierea formulei';
						5 : result:= '0 la o putere negativa este nedefinit';
						6 : result:= 'Un numar negativ la o putere reala are ca rezultat'+
							' un numar complex';
						7 : result:= 'Radical dintr-un numar negativ are ca rezulatat'+
							' un numar complex';
						8 : result:= 'Logaritm de 0 este nedefinit';
						9 : result:= 'Logaritm de un numar negative are ca rezultat'+
							' un numar complex';
						10: result:= 'Tangenta este nedefinita pentru valori care sunt'+
							' multipli de pi/2 (90 de grade)';
						11: result:= 'Cotangenta este nedefinita pentru valori care sunt'+
							' multiplie de pi (180 de grade)';
						12: result:= 'Arcsin merge doar pe intervalul [-1, 1] (radiani)';
						13: result:= 'Arccos merge doar pe intrevalul [-1, 1] (radiani)';
						14: result:= 'Ctgh de 0 este nedefinita';
						15: result:= 'Arccosh merge doar pe intervalul (-∞, -1) ∩ [1, +∞)';
						16: result:= 'Arctgh merge doar pe intervalul (-1, 1) (radiani)';
						17: result:= 'Arcctgh merge doar pe intervalul (-∞, -1) ∩ (1, +∞)';
						30: result:= 'Metoda de evaluare necunoscuta: ';
						31: result:= 'Numar necunoscut: ';
						32: result:= 'Numarul introdus nu e in intervalul [0..15]: ';
						33: result:= 'Limba necunoscuta: ';
						34: result:= 'Unitate necunoscuta: ';
						35: result:= 'Setare necunoscuta: ';
						40: result:= 'Memorie plina';
						50: result:= 'Domeniul functiei trebuie sa fie intre -50 si 50.';
						51: result:= 'Marimea graficului trenuie sa fie intre 5 si 50.';
					end;	// case 'err' end
			end;	// case 'settings^.language end
		end;


	function raise_error_gui(settings: pprogramSettings; err: byte): string;
		begin
			case settings^.language of
				1: result:= concat('Error: ', return_error_extended(settings, err));
				2: result:= concat('Eroare: ', return_error_extended(settings, err));
			end;
		end;


	procedure raise_error_cli(settings: pprogramSettings; err: byte);
		begin
			case settings^.language of
				1 : writeln('--> Error: ', return_error_extended(settings, err));
				2 : writeln('--> Eroare: ', return_error_extended(settings, err));
			end;
		end;
		

	procedure raise_error_startup(settings: pprogramSettings; err: byte; additionalArgs: string);
		begin
			case settings^.language of
				1: writeln('--> Error: ', return_error_extended(settings, err), '"',
					additionalArgs, '"');
				2: writeln('--> Eroare: ', return_error_extended(settings, err), '"',
					additionalArgs, '"');
			end;
		end;
end.