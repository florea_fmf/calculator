{
	 main -- main program
	 version 1.0, November 17th, 2012

	 Copyright (C) 2012-2013 Florea Marius Florin

	 This software is provided 'as-is', without any express or implied
	 warranty.  In no event will the authors be held liable for any damages
	 arising from the use of this software.

	 Permission is granted to anyone to use this software for any purpose,
	 including commercial applications, and to alter it and redistribute it
	 freely, subject to the following restrictions:

			1. The origin of this software must not be misrepresented; you must not
				claim that you wrote the original software. If you use this software
				in a product, an acknowledgment in the product documentation would be
				appreciated but is not required.
			2. Altered source versions must be plainly marked as such, and must not be
				misrepresented as being the original software.
			3. This notice may not be removed or altered from any source distribution.

	Florea Marius Florin, florea.fmf@gmail.com
}

(* main program - starts the program in gui mode if not
* specified otherwise by command line arguments. Also here
* is handled the cli of the program *)


{$mode objfpc}
{$h+}
{$smartlink on}


program main;


uses
	sysutils, crt,
	myTypes, error, calculator, gui;


var
	settings: pprogramSettings;
	data: pcompData;


(* returns the mode the program should be started with;
* if there are any parameters then the mode is cli
* else the mode is gui *)
function get_start_mode(): byte;
	begin
		if (paramCount = 0) then begin
			result:= 1;
		end else begin
			result:= 2;
		end;
	end;


(* returns the string containing the string to be evaluated;
* only in mode = 2 (cli) *)
function get_input(): string;
	begin
		result:= paramStr(paramCount);
	end;


(* gets the cli mode, interactive || passive *)
function get_cliMode(): byte;
	var
		i: byte;
	begin
		for i:= 1 to paramCount do
			if (paramStr(i) = '-i') then begin
				result:= 2;
				break;
			end else
				result:= 255;	// 255 is returned if that switch doesn't exists
	end;


(* get evaluation mode *)
function get_eval(): byte;
	var
		i: byte;
	begin
		for i:= 1 to paramCount do
			if (paramStr(i) = '-e') then begin
				case (paramStr(i+1)) of
					'infix' : result:= 1;
					'postfix': result:= 2;
				else begin
					raise_error_startup(settings, 30, paramStr(i+1));
					halt(-1);
				end;
				end;
				break;
			end else
				result:= 255;
	end;


(* get the decimals to which should the result be computed *)
function get_decimals(): byte;
	var
		i, err: byte;
		d: integer;
	begin
		d:= 0; err:= 0;
		for i:= 1 to paramCount do
			if (paramStr(i) = '-d') then begin
				val(paramStr(i+1), d, err);
				if (err <> 0) then begin
					raise_error_startup(settings, 31, paramStr(i+1));
					halt(-1);
				end else if ((d < 0) or (d > maxDecimals)) then begin
					raise_error_startup(settings, 32, paramStr(i+1));
					halt(-1);
				end;
				result:= d;
				break;
			end else
				result:= 255;
	end;


(* gets the language *)
function get_language(): byte;
	var
		i: byte;
	begin
		for i:= 1 to paramCount do
			if (paramStr(i) = '-l') then begin
				case (paramStr(i+1)) of
					'en', 'english' : result:= 1;
					'ro', 'romana' : result:= 2;
				else begin
					raise_error_startup(settings, 33, paramStr(i+1));
					halt(-1);
				end;
				end;
				break;
			end else
				result:= 255;
	end;


(* gets the trig units *)
function get_trigUnits(): byte;
	var
		i: byte;
	begin
		for i:= 1 to paramCount do
			if (paramStr(i) = '-u') then begin
				case (paramStr(i+1)) of
					'deg', 'degrees', 'angles', 'ang', 'grade' : result:= 1;
					'rad', 'radians', 'radiani' : result:= 2;
				else begin
					raise_error_startup(settings, 34, paramStr(i+1));
					halt(-1);
				end;
				end;
				break;
			end else
				result:= 255;
	end;


(* if the -h param is given then print the help and exit *)
procedure get_help();
	var
		i: integer;
	begin
		for i:= 1 to paramCount do
			if (paramStr(i) = '-h') then begin
				case (settings^.language) of
					1: writeln('Usage: calculator [parameters] "[expresion to be evaluated]"'+#10+
						'[parameters]:'+#10+
						#9+'no parameters = program is started in gui mode'+#10+
						#9+'-i = starts the program in interactive mode (like bc)'+#10+
						#9+'-e [infix/postfix] = how to evaluate the string'+#10+
						#9+'-d [0..15] = the number of decimals used to compute the result to'+#10+
						#9+'-l [en/ro] = language'+#10+
						#9+'-u [deg/rad] = units used for computing trigonometric'+#10+
						#9+#9+'functions (deg = degrees, rad = radians)'+#10+
						#9+'-h = this help'+#10+
						#10+'Sample usage: '+#10+
						#9+'calculator -d 7 -u rad "sin(4)"'+#10+
						#9+'calculator -e postfix "3 4 +"'+#10+
						#9+'calculator -i -d 7'+#10+
						#9+'calculator -edl infix 7 ro "4+5"'+#10+
						#9+'calculator -id 7'+#10+
						#10#9+'Note: the "i" param must be always first (if used)'+#10+
						#9+'Note: calculator -el infix ro -d 7 "1+3"; does not work');
					2: writeln('Mod folosire: calculator [parametrii] "[expresia de evaluat]"'+#10+
						'[parametrii]:'+#10+
						#9+'fara parametrii = programul este pornit in mod vizual'+#10+
						#9+'-i = porneste programul in mod interactiv (asemenea bc)'+#10+
						#9+'-e [infix/postfix] = metoda de evaluare folosita'+#10+
						#9+'-d [0..15] = numarul de zecimale afisat la rezultat'+#10+
						#9+'-l [en/ro] = limba'+#10+
						#9+'-u [grade/radiani] = unitatea folosita ca argument pentru functiile'+#10+
						#9#9+'trigonometrice'+#10+
						#9+'-h = acest mesaj'+#10+
						#10+'Exemple de folosire: '+#10+
						#9+'calculator -d 7 -u radiani "sin(4)"'+#10+
						#9+'calculator -e postfix "3 4 +"'+#10+
						#9+'calculator -i -d 7'+#10+
						#9+'calculator -edl infix 7 ro "4+5"'+#10+
						#9+'caluclator -id 7'+#10+
						#10#9+'Nota: parametrul "i" trebuie sa fie mereu primul (daca e folosit)'+#10+
						#9+'Nota: calculator -el infix ro -d 7 "1+3"; nu merge');
				end;
				halt(0);
			end;
	end;


(* returns true if the paramters given to the program are "condensed"
* which means that they are like: -edl infix 7 ro "3+4"; hope I was clear enough *)
function is_condensed(): boolean;
	begin
		if ((paramStr(1)[1] = '-') and (length(paramStr(1)) > 2)) then
			result:= true
		else
			result:= false;
	end;


procedure get_settings_condensed(sett: pprogramSettings);
	var
		d: byte;
		paramI: boolean = false;	// if the 'i' parameter is in the string
		d2, err: integer;
	begin
		if (pos('i', paramStr(1)) > 0) then begin
			// cli is to be started in interactive mode
			sett^.cliMode:= 2;
			sett^.mode:= 2;
			paramI:= true;
		end;

		if (pos('e', paramStr(1)) > 0) then begin
			// the eval setting is changed/set
			d:= pos('e', paramStr(1));
			if (not(paramI)) then
				case (paramStr(d)) of
					'infix' : sett^.eval:= 1;
					'postfix': sett^.eval:= 2;
				else begin
					raise_error_startup(sett, 30, paramStr(d));
					halt(-1);
				end;
				end
			else
				case (paramStr(d-1)) of
					'infix' : sett^.eval:= 1;
					'postfix': sett^.eval:= 2;
				else begin
					raise_error_startup(sett, 30, paramStr(d-1));
					halt(-1);
				end;
				end
		end;

		if (pos('d', paramStr(1)) > 0) then begin
			// the decimals setting is set
			d:= pos('d', paramStr(1));
			if (not(paramI)) then begin
				val(paramStr(d), d2, err);
				if (err <> 0) then begin
					raise_error_startup(sett, 31, paramStr(d));
					halt(-1);
				end else if ((d2 < 0) or (d2 > maxDecimals)) then begin
					raise_error_startup(sett, 32, paramStr(d));
					halt(-1);
				end else
					sett^.decimals:= d2;
			end else begin
				val(paramStr(d-1), d2, err);
				if (err <> 0) then begin
					raise_error_startup(sett, 31, paramStr(d-1));
					halt(-1);
				end else if ((d2 < 0) or (d2 > maxDecimals)) then begin
					raise_error_startup(sett, 32, paramStr(d-1));
					halt(-1);
				end else
					sett^.decimals:= d2;
			end;
		end;

		if (pos('l', paramStr(1)) > 0) then begin
			// the language setting is set
			d:= pos('l', paramStr(1));
			if (not(paramI)) then
				case (paramStr(d)) of
					'en', 'english' : sett^.language:= 1;
					'ro', 'romana' : sett^.language:= 2;
				else begin
					raise_error_startup(sett, 33, paramStr(d));
					halt(-1);
				end;
				end
			else
				case (paramStr(d-1)) of
					'en', 'english' : sett^.language:= 1;
					'ro', 'romana' : sett^.language:= 2;
				else begin
					raise_error_startup(sett, 33, paramStr(d-1));
					halt(-1);
				end;
				end;
		end;

		if (pos('u', paramStr(1)) > 0) then begin
			// the trigUnits settings is set
			d:= pos('u', paramStr(1));
			if (not(paramI)) then
				case (paramStr(d)) of
					'deg', 'degrees', 'angles', 'ang', 'grade' : sett^.trigUnits:= 1;
					'rad', 'radians', 'radiani' : sett^.trigUnits:= 2;
				else begin
					raise_error_startup(sett, 34, paramStr(d));
					halt(-1);
				end;
				end
			else
				case (paramStr(d-1)) of
					'deg', 'degrees', 'angles', 'ang', 'grade' : sett^.trigUnits:= 1;
					'rad', 'radians', 'radiani' : sett^.trigUnits:= 2;
				else begin
					raise_error_startup(sett, 34, paramStr(d-1));
					halt(-1);
				end;
				end;
		end;
	end;


procedure set_defaults(sett: pprogramSettings; dat: pcompData);
	var
		f: file of byte;
		d: byte;
	begin
		with sett^ do begin
			cliMode:= 1;
			mode:= get_start_mode();
			eval:= 1;
			decimals:= 2;
			language:= 1;
			trigUnits:= 1;
			if (mode = 2) then begin
				get_help();
				
				if (is_condensed()) then
					// get all parameters here
					get_settings_condensed(sett)
				else begin
					// get here the parameters if any
					d:= get_cliMode();
					if (d <> 255) then
						cliMode:= d;
					d:= get_eval();
					if (d <> 255) then
						eval:= d;
					d:= get_decimals();
					if (d <> 255) then
						decimals:= d;
					d:= get_language();
					if (d <> 255) then
						language:= d;
					d:= get_trigUnits();
					if (d <> 255) then
						trigUnits:= d;
				end;
			end;

			{$ifdef unix}
				// we may not have write permission where the program is installed
				settingsFile:= getUserDir + '.config/calcSettings';
			{$endif}
			{$ifdef windows}
				settingsFile:= 'settings';
			{$endif}
		end;

		with settings^ do begin
			assign(f, settingsFile);
			rewrite(f);
			write(f, mode, cliMode, eval, decimals, language, trigUnits);
			close(f);
		end;

		with dat^ do begin
			if (settings^.mode = 1) then begin
				input:= '';
			end else begin
				input:= get_input();
			end;
			res:= '';
			err:= 0;
			suppressErr:= 1;
		end;
	end;


(* reads the settings from the 'settings' file,
* located in the same folder with the program 
* on windows, and also looks in ~/.config/calcSettings
* for the file on linux *)
procedure read_settings();
	var
		d: byte;
		f: file of byte;
		s: string;
		decimalsSetted: boolean = false;
	begin
		assign(f, settings^.settingsFile);
		reset(f);

		with settings^ do
			read(f, mode, cliMode, eval, decimals, language, trigUnits);

		close(f);

		if (paramCount > 0) then
			with settings^ do begin
				get_help();
				if (is_condensed()) then
					// get all parameters here
					get_settings_condensed(settings)
				else begin
					// get here the parameters if any
					d:= get_cliMode();
					if (d <> 255) then
						cliMode:= d;
					d:= get_eval();
					if (d <> 255) then
						eval:= d;
					d:= get_decimals();
					if (d <> 255) then begin
						decimals:= d;
						decimalsSetted:= true;
					end;
					d:= get_language();
					if (d <> 255) then
						language:= d;
					d:= get_trigUnits();
					if (d <> 255) then
						trigUnits:= d;
				end;
			end;

		with data^ do begin
			if ((settings^.mode = 1) and (paramCount = 0)) then begin
				input:= '';
			end else begin
				input:= get_input();

				// check to see if we have something to compute
				// if yes then the program is in cli mode
				err:= 0;
				s:= input;
				compute_format(settings, data);
				if (err = 0) then begin
					// if the last parameter is the one setting the decimals
					// then the program confuses it with input to be processed
					// and wrongly starts in cli mode
					if (decimalsSetted) then begin
						val(s, d);
						if (settings^.decimals <> d) then begin
							settings^.mode:= 2;
							input:= s;
						end;
					end;
				end;
				
				input:= s;
				if (get_cliMode = 255) then
					settings^.mode:= 2;
			end;
			res:= '';
			err:= 0;
			suppressErr:= 1;
		end;
	end;


(* starts the program in cli/gui *)
procedure start(sett: pprogramSettings; dat: pcompData);
	var
		d: byte;
		s: string;
		d2, err: integer;
	begin
		if (sett^.mode = 1) then
			start_gui(sett, dat)
		else
			case (sett^.cliMode) of
				1 : begin
						compute_format(sett, dat);
						writeln(dat^.res);
					end;
				2 : begin
						writeln('Type "q" or "quit" to quit', #10);
						while (true) do begin
							readln(dat^.input);

							if ((dat^.input = 'q') or (dat^.input = 'quit')) then
								// if the user want's to quit
								break
							else if (pos('clear', dat^.input) > 0) then begin
								// clear whatever is on screen
								clrscr;
								continue;
							end else if (pos('set', dat^.input) > 0) then begin
								// if the user wants to change some setting
								if (pos('eval', dat^.input) > 0) then begin
									// if the eval setting is being changed
									d:= pos('eval', dat^.input);
									s:= copy(dat^.input, d+5, length(dat^.input));
									case (s) of
										'infix' : sett^.eval:= 1;
										'postfix' : sett^.eval:= 2;
									else
										raise_error_startup(sett, 30, s);
									end;	// end case
								end else if (pos('decimals', dat^.input) > 0) then begin	// end if 'eval'
									// if the decimals setting is being changed
									d:= pos('decimals', dat^.input);
									s:= copy(dat^.input, d+9, length(dat^.input));
									val(s, d2, err);
									if (err <> 0) then
										raise_error_startup(sett, 31, s)
									else
										case (d2) of
											0..7 : sett^.decimals:= d2;
										else
											raise_error_startup(sett, 32, s);
										end;
								end else if (pos('language', dat^.input) > 0) then begin	// end if 'decimals'
									// if the language settings is being changed
									d:= pos('language', dat^.input);
									s:= copy(dat^.input, d+9, length(dat^.input));
									case (s) of
										'en', 'english' : sett^.language:= 1;
										'ro', 'romana' : sett^.language:= 2;
									else
										raise_error_startup(sett, 33, s);
									end;
								end else if (pos('units', dat^.input) > 0) then begin	// end if 'language'
									// if the trigUnits is being changed
									// NOTE: here we can get away with using only 'units'
									// because the only thing taking 2 units is the trigUnits
									d:= pos('units', dat^.input);
									s:= copy(dat^.input, d+6, length(dat^.input));
									case (s) of
										'deg', 'degrees', 'angles', 'ang', 'grade' : sett^.trigUnits:= 1;
										'rad', 'radians', 'radiani' : sett^.trigUnits:= 2;
									else
										raise_error_startup(sett, 34, s);
									end;
								end else	// end if 'units'
									raise_error_startup(sett, 35, s);

								continue;	// to skip computing, as it will result in an error
							end;	// end if 'set'

							compute_format(sett, dat);
							if (dat^.err = 0) then
								// there was no error during computation, so print the result
								writeln(dat^.res)
							else
								// to reset the error for next pass, otherwise we will keep
								// getting the same error message over and over again
								dat^.err:= 0;
						end;
					end;
			end;
	end;


begin
	new(settings);
	new(data);

	// NOTE: command line arguments have higher priority than
	// those in the settings file, therefore cli args will
	// overwrite their settings counterpart;
	// the order the data is saved in file is as follows:
	// mode, cliMode, eval, decimals, language, trigUnits
	if (fileExists('settings')) then begin
		settings^.settingsFile:= 'settings';
		read_settings();
	end else if (fileExists(getUserDir + '.config/calcSettings')) then begin
		settings^.settingsFile:= getUserDir + '.config/calcSettings';
		read_settings();
	end else
		set_defaults(settings, data);

	{$ifdef unix}
		settings^.tempDirectory:= '/tmp/calcGraph.jpg';
	{$endif}
	{$ifdef windows}
		settings^.tempDirectory:= GetEnvironmentVariable('TEMP')+'\calcGraph.jpg';
	{$endif}


	start(settings, data);


	// clean up
	if (fileExists(settings^.tempDirectory)) then
		deleteFile(settings^.tempDirectory);

	dispose(settings);
	dispose(data);
end.
