Calculator program (or a bad clone at it)
=========================================

---

Info
----
	* A calculator with basic and advanced interfaces
	which can compute ecuations in infix format and postfix format.
	* works on unix-like (no dependencies except those of fpc and FpGui)
	and Windows os (requires freetype-6.dll).

---
	
Compile
-------
	> Note: compiler version used is: 2.6.0.
	> Note: fpGui version used is: 0.8.
	* First patch fpGui (fpGuiDir/src/gui/fpg_edit.pas and
	(fpGuiDir/src/corelib/x11/fpg_xft_x11.pas)) with the supplied patch file.
	* Make sure the compiler can find the fpGui libs.
	* On unix-like systems use the supplied script.
	* On windows use (from src dir):
		fpc -WG -O2 -Xs -Cg -Fuunits main.pas

---

Disclaimer
----------
	* This program is supposed to be my project for the highschool
	(useless) certificate. (god this makes no sense to outsiders
	(pentru romani: asta e atestatul meu)).
	* The code is (at it's best) crap. "Then why use our server space
	asshole?", "Good question. Let's see. Mainly because nothing is useless,
	it can (and I hope it will) serve as a bad example."
	* The compile script on gnu/linux has no failsaves whatsoever.


---

Pics or it didn't happen
------------------------
	http://imgur.com/a/OvJjH
