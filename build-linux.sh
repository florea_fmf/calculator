#!/bin/bash

srcDir="src"
releaseDir="release"
unitPath="src/units"


echo "Checking for source directory"
if [ -d "$srcDir" -a -w "$srcDir" ]; then
	echo "Source directory found. Continue building"
	echo ""
else
	echo "Source directory not found. Halting"
	exit
fi

if [ -d "$releaseDir" -a -w "$releaseDir" ]; then
	fpc -O2 -Xs -Cg -Fu"$unitPath" -FE"$releaseDir" "$srcDir/main.pas"
	cd "$releaseDir"
	rm *.o *.ppu *.a
	mv main calculator
else
	mkdir "$releaseDir"
	fpc -O2 -Xs -Cg -Fu"$unitPath" -FE"$releaseDir" "$srcDir/main.pas"
	cd "$releaseDir"
	rm *.o *.ppu *.a
	mv main calculator
fi

echo ""
echo "Build finnished" 


exit